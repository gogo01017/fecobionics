function [avgPress,avgAcc, gyroCaliFlag] = autoCalibrateProbe(serialObj)
%startCommand(serialObj);
[serialObj, successFlag] = initializeSerial(serialObj);
[pressure, gyro, temp , acc] = getDataForCalibration(serialObj,99)
%stopCommand(serialObj);

avgPress = mean(pressure')';
avgAcc = mean(acc')';
if isnan(avgPress)
    disp('adfasf')
end
%not made gyrocalibration yet. 
gyroCaliFlag = 0; 
