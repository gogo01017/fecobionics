function [timeSample,gyro,gyroSampleTime,pressure,temperature,acc] = unpackData(intvalues)
% Syntax:
%    [timeSample,gyro,gyroSampleTime,pressure,temperature] = unpackData(intvalues)
%
% Unpacks the data from the fecobionics. Protocol 2. 
% By FLU, 25/11-2015
% Filer version: 1.0. 

numOfPackages = size(intvalues,2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%% Sample data %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Extract sample time:
idxT = fliplr(4:7);
timeSample = uint8(intvalues(idxT,:));
timeSample = double(typecast(timeSample(:), 'INT32'))/1000;
timeSample = timeSample';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Extract Gyr&Acc data %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%get Idx'es
idxGFront = [fliplr(8:11) fliplr(12:15) fliplr(16:19) fliplr(20:23)];
idxGBack = idxGFront + 26;
%idxGWTU = idxGFront + 52;
idxGyro = [idxGFront idxGBack ];

idxAF = [fliplr(24:25) fliplr(26:27) fliplr(28:29)];
idxAB = idxAF+26;
%idxAWTU = idxAF+52;
idxAcc = [idxAF idxAB];
idxGyroSampleTime = [fliplr(30:33) fliplr(30:33)+26];

%extract data from idx
gyroSampleTime = uint8(intvalues(idxGyroSampleTime,:));
gyroSampleTime = double(typecast(gyroSampleTime(:), 'INT32'))/1000;
gyroSampleTime = reshape(gyroSampleTime,2,numOfPackages);

gyro = uint8(intvalues(idxGyro,:));
gyro = double(typecast(gyro(:), 'INT32'));
gyro = reshape(gyro,4*2,numOfPackages);

acc = uint8(intvalues(idxAcc,:));
acc = double(typecast(acc(:), 'INT16'));
acc = reshape(acc,3*2,numOfPackages); 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% Pressure data %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
idxPressure = [86:2:90];
%idxPressure = [60:2:64];
idxPressureDeci = [87:2:91];
%idxPressureDeci = [61:2:65];
pressure = intvalues(idxPressure,:)+intvalues(idxPressureDeci,:)/100;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Temperature data %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
idxTemp = [92:2:96];
%idxTemp = [66:2:70];
idxTempDeci = [93:2:97];
%idxTempDeci = [67:2:71];
temperature = intvalues(idxTemp,:)+intvalues(idxTempDeci,:)/10;
