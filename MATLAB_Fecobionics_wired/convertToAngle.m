function [angles] = convertToAngle(gyro)
% syntax:
%  [angles] = convertToAngle(gyro)
%
% Version 1.0
% FLU: 25/11-2015
% Converts quart to angles 
% Made to work with matrices, from Jeppes work. 

angles = reshape(gyro,size(gyro,2)*2,4);
angles = (180/pi)*(quat2eul(angles));
angles = reshape(angles,3*2,size(gyro,2));
