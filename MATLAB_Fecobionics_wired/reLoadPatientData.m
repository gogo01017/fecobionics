function [handles] = loadPatientTable(handles)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

load('patientData');
handles.patientName = patientData(:,1);
handles.patientID = patientData(:,2);
handles.patientComment = patientData(:,6);

for i=1:length(handles.patientName)
    patientInformation(i,1) = handles.patientName(i);
    patientInformation(i,2) = handles.patientID(i);
    patientInformation(i,5) = handles.patientComment(i);
    filename = strcat(handles.patientID, '.mat');
    
    pathToExaminiations =  Resources('pathToExaminiations');
    if exist([pathToExaminiations filename{i}], 'file')
        files = load([pathToExaminiations filename{i}]);
            patientInformation(i,3) = {num2str(length(files.data))};
            date = files.data(end).startTime;
            patientInformation(i,4) = {date(1:end)};
        end
    end
    
    formatedPatientTableData = formatUItable(patientInformation,[nan]);
    handles.patientTableData = patientInformation;
    set(handles.patientTable,'Data',formatedPatientTableData);
end


