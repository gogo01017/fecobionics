function varargout = About(varargin)
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @About_OpeningFcn, ...
                   'gui_OutputFcn',  @About_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before About is made visible.
function About_OpeningFcn(hObject, ~, handles, varargin)
handles.output = hObject;
set(handles.text5,'String','GIOME, All rights reserved 2015')
set(handles.swVersinText,'String','2.0')

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = About_OutputFcn(~, ~, handles) 
% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in okButton.
function okButton_Callback(hObject, ~, ~)
delete(get(hObject, 'parent')); %Closing this window
