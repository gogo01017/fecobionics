function commentLinesAnalysis(handles)
chosenexamnination = handles.selectExamination;
filename = strcat(handles.patientID, '.mat');
%FLU
pathToExaminiations = Resources('pathToExaminiations');
alldata = load([pathToExaminiations filename]);

examinationsdata = alldata.data(chosenexamnination);

ax1=handles.pressPlot;
ax2=handles.anglePlot;
ax=[ax1 ax2];

values = [-10 120; -180 180];

sizeofcomment=size([examinationsdata.commentData.commentData]);
for j=1:2
    axes(ax(j))
    for i=1:sizeofcomment(1,1)
        xcomment=examinationsdata.commentData.commentData(i,1);
        xnum=str2double(xcomment{1});
        stringcomment=examinationsdata.commentData.commentData(i,2);
        comment=stringcomment{1};
        x=[xnum xnum];
        y = values(j, :);
        CommentPlot=plot(x,y,'LineWidth',3,'Color',[0.8 0.8 0.8]);
        
        hcmenu = uicontextmenu;
        if(isempty(comment))
            uimenu(hcmenu, 'Label', '');
        else
            uimenu(hcmenu, 'Label', comment);
            set(CommentPlot, 'uicontextmenu', hcmenu);
        end
    end
end

end

