function stopCommand(serialobj)
char dataarray[99]; %st�rrelse i forhold til protokollen
dataarray(1)=171; %Preamble er AB i hex, dvs 171 i decimal tal
dataarray(2)=3; %adresse = probe
%dataarray(2)=2; %adresse = WTU - skal i det endelige produkt....
dataarray(3)=2; %2= stop command
dataarray(4:97)=0; % der er intet data med
%udregn checksum p� datapakken undtagen de to sidste bytes
[~,sum1, sum2] = CheckSum(dataarray,97); %er ligeglad med den f�rste v�rdi
dataarray(98)=sum2;
dataarray(99)=sum1;

comReqst = instrfind;
openComs = 0;
if ~isempty(comReqst)
    comStatus = comReqst.status;
    for i = 1:size(comReqst,2)
        if iscell(comStatus)
            if ~isequal(comStatus{i},'closed')
                openComs = 1;
            end
        else
            if ~isequal(comStatus,'closed')
                openComs = 1;
            end
        end
    end
end


try
   if openComs
        fwrite(serialobj, dataarray);
        disp('The connection is closed')
    end
catch
    disp('Fail')
    
end
end

