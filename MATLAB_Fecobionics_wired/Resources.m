function output = Resources(RessourceID,varargin)
%finds the path to the database, must be where all the matlab files are in
%varargin is to pass on the patientID.
%a subfolder. 
p = mfilename;
fullPath = mfilename('fullpath');
dataBasePath = [strrep(fullPath,p,''),'Databases\'];
if ~isempty(varargin)
handles.patientID = varargin{1};
else
    handles.patientID = 'Error';
end
%dataBasePath = 'C:\Users\GIOME\Documents\MATLAB\Version 2.1\Databases\';
%finds the path to the data folder. Must be on the desktop. 
dataPath = [winqueryreg('HKEY_CURRENT_USER', 'Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders', 'Desktop'), '\FecoData\'];
switch RessourceID
    case 'dataPath'
        output = dataPath;
    case 'baseDataPath'
        output = dataBasePath;
    case 'pathToTemp'
        output= [dataPath, 'Temporary\'];
    case 'pathToExaminiations'
        output= [dataPath, 'Examiniations\'];
    case 'fullPathToCommentData'
        output= [dataBasePath, 'commentData.mat'];    
    case 'getExamNr'
        %output= load([dataBasePath, 'examnNo.mat']);
        %output = output.examnNr;
        patientID= varargin{1};
        p = dir([dataPath, 'Temporary\*.mat']);
		output = length(p)+1
   case 'updateExamnNr'
       disp('denne funktion b�r ikke blive brugt l�ngere..Ressources line 33')
      %  output= load([dataBasePath, 'examnNo.mat']);
      %  output = output.examnNr+1;
      %  examnNr = output;
      %  save([dataBasePath, 'examnNo.mat'],'examnNr');
   case 'getPatientID'
        tempTime   = datestr(now, 'yyyymmdd_HHMMSS');
        output = ['PatientID_' tempTime];
   case 'updatePatientID'
%        output= load([dataBasePath, 'patientID.mat']);
%        output = output.patientID+1;
%        patientID = output;
%        save([dataBasePath, 'patientID.mat'],'patientID');
disp('should not be used. Resources.m, line 47')
   case 'getTempNo'
		p = load([dataPath 'Examiniations\' handles.patientID '.mat']);
        if isfield(p,'data')
            output = size(p.data,2)+1;
        else
            output = 1; 
        end
% 		output= load([dataBasePath, 'TempNo.mat']);
%         output = output.TempNo;
   case 'updateTempNo'
  %      output= load([dataBasePath, 'TempNo.mat']);
  %      output = output.TempNo+1;
  %      TempNo = output;
  %      save([dataBasePath, 'TempNo.mat'],'TempNo');
  disp('should not be used')
    case 'getTransferFunction'
        output= load([dataBasePath, 'transferFunctionForBend.mat']);
        output = output.transfer;
end

