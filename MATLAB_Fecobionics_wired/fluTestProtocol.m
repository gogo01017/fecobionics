%FLU test af protocol:

[serialobj] = createSerialPort;
startCommand(serialobj);
pause(2);
if serialobj.BytesAvailable<500
    %Forventer at freq er h�jere end 10, s� er 1 sekund nok. 
    pause(2)
    %put in user feedback --> noget med at den henter data
end

%align buffer:
[intvalues,~,~]=fread(serialobj,500,'uint8');

idx = find(intvalues==hex2dec('AB'));
idxStart = find(diff(idx)==99);
idxStart = idx(idxStart(end))+99; %for det er en diff, og s� er den sidste i r�kken ikke med. 

extraBytes = 99-(500-idxStart)-1;
[~,~,~]=fread(serialobj,extraBytes,'uint8');

numberOfPackages = floor(serialobj.BytesAvailable/99);

[intvalues,~,~]=fread(serialobj,numberOfPackages*99,'uint8');

%Check if preable is first:
data = reshape(intvalues,99,numberOfPackages)';

%check if all starts with a preamble
if ~isequal(size(find(data(:,1)==hex2dec('AB')),1),numberOfPackages)
    disp('redo alignment... noget gik galt')
    break
end

%% convert time
idxT = fliplr([4:7]); %fliplr because matlab reads most significant bit diffetn than the embedded SW. 
time = data(:,idxT);
time = time';
time = typecast(uint8(time(:))','INT32');

%% Convert Pressure
PressFront = data(:,86)+data(:,87)/100;%/0.098063799; %regner om til cm H2O;
PressBag = (data(:,88)+data(:,89)/100);%/0.098063799; %regner om til cm H2O;
PressRare = (data(:,90)+data(:,91)/100);%/0.098063799; %regner om til cm H2O;

%% Convert temperature
tempFront = data(:,92)+data(:,93)/10;
tempBag = data(:,94)+data(:,95)/10;
tempRear = data(:,96)+data(:,97)/10;

%Convert CheckSum
CheckSum




%tic;
%set(serialobj,'BytesAvailableFcn',{@SerialReadCallbackFLU}) % kalder funktionen SerialReadCallback n�r bytes er tilstede

