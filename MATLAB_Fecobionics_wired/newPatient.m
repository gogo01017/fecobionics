function varargout = newPatient(varargin)
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @newPatient_OpeningFcn, ...
                   'gui_OutputFcn',  @newPatient_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before newPatient is made visible.
function newPatient_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;

handles.getPatientID = Resources('getPatientID');
set(handles.IDText, 'String', handles.getPatientID);

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = newPatient_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

% --- Executes on button press in cancleButton.
function cancleButton_Callback(hObject, eventdata, handles)
delete(get(hObject, 'parent'));


% --- Executes on button press in okButton.
function okButton_Callback(hObject, eventdata, handles)
newPatientFunction(handles)

function NameText_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function NameText_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function IDText_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function IDText_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function DOBText_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function DOBText_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function DoctorText_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function DoctorText_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function CommentsText_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function CommentsText_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Menuer --------------------------------------------------------------------
function Menu_Callback(hObject, eventdata, handles)

function AboutHead_Callback(hObject, eventdata, handles)

function About_Callback(hObject, eventdata, handles)
About();

function Logout_Callback(hObject, eventdata, handles)
logoutDialog();

function Exit_Callback(hObject, eventdata, handles)
closeProgram();
