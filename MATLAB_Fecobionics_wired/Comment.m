function Comment(key, handles, hObject)
set(handles.serialobj,'BytesAvailableFcn','')

if ~isempty(handles.pressure) %the probe did not start    
    global comment3global
    global comment4global
    global comment5global
    
    ax1=handles.pressPlot;
    ax2=handles.anglePlot;
    ax=[ax1 ax2];
    values = [-10 120; -180 180];
    
    if(isequal(key ,'f6'))
        Comment6(num2str(handles.timeX(end)));
        for j=1:2
            axes(ax(j))
            x=[handles.timeX(end) handles.timeX(end)];
            y = values(j, :);
            plot(x,y,'LineWidth',3,'Color',[0.8 0.8 0.8]);
        end
    else if (isequal(key ,'f1') || isequal(key ,'f2') || isequal(key ,'f3') || isequal(key ,'f4') || isequal(key ,'f5'))
            switch key
                case 'f1'
                    set(handles.KeypressText, 'String', [key ': The patient is on the toilet'])
                case 'f2'
                    set(handles.KeypressText, 'String', [key ': The patient is off the toilet'])
                case 'f3'
                    set(handles.KeypressText, 'String', [key ': ' comment3global])
                case 'f4'
                    set(handles.KeypressText, 'String', [key ': ' comment4global])
                case 'f5'
                    set(handles.KeypressText, 'String', [key ': ' comment5global])
            end
            
            set(handles.KeypressText, 'visible', 'on');
            drawnow;
            pause(2);
            
            for j=1:2
                axes(ax(j))
                x=[handles.timeX(end) handles.timeX(end)];
                y = values(j, :);
                plot(x,y,'LineWidth',3,'Color',[0.8 0.8 0.8]);
            end
            drawnow;
            
            set(handles.KeypressText, 'visible', 'off');
        else if  isequal(key, 'f11')
                vol = get(handles.volumeText, 'String');
                vol = str2double(vol);
                
                if vol > 0
                    volume = vol-10;
                    volText = num2str(volume);
                    set(handles.volumeText, 'String', volText);
                    set(handles.KeypressText, 'String', ['The volume is: ' volText ' ml'])
                    set(handles.KeypressText, 'visible', 'on');
                    drawnow;
                    pause(2);
                    
                    for j=1:2
                        axes(ax(j))
                        x=[handles.timeX(end) handles.timeX(end)];
                        y = values(j, :);
                        plot(x,y,'LineWidth',3,'Color',[0.8 0.8 0.8]);
                    end
                end
                
            else if isequal(key, 'f12')
                    vol = get(handles.volumeText, 'String');
                    
                    if isempty(vol)
                        vol=0;
                    else
                        vol = str2double(vol);
                    end
                    
                    volume = vol+10;
                    volText = num2str(volume);
                    set(handles.volumeText, 'String', volText);
                    set(handles.KeypressText, 'String', ['The volume is: ' volText ' ml'])
                    set(handles.KeypressText, 'visible', 'on');
                    drawnow;
                    pause(2);
                    
                    for j=1:2
                        axes(ax(j))
                        x=[handles.timeX(end) handles.timeX(end)];
                        y = values(j, :);
                        plot(x,y,'LineWidth',3,'Color',[0.8 0.8 0.8]);
                    end
                    
                    set(handles.KeypressText, 'visible', 'off');
                    set(handles.volumeText, 'UserData', volText);
                end
            end
        end
    end
    
    
    %Instert into the comment table ------------------------
    comment= 0;
    switch key
        case 'f1'
            comment = 'The patient is on the toilet';
        case 'f2'
            comment = 'The patient is off the toilet';
        case 'f3'
            comment = comment3global;
        case 'f4'
            comment = comment4global;
        case 'f5'
            comment = comment5global;
        case 'f12'
            comment = ['The volume is: ' volText ' ml'];
    end
    if ~isequal(comment,0)
        data = load('commentData.mat');
        %warning ('off','all');
        insert = matfile('commentData.mat', 'Writable', true);
        %warning ('on','all');
        newComment = {num2str(handles.timeX(end)), comment};
        
        if strcmp(data.commentData(1,1),'') == 1
            insert.commentData(1,:) = newComment;
        else
            insert.commentData(end+1,:) = newComment;
        end
    end
    drawnow();
    
    set(handles.serialobj,'BytesAvailableFcn',{@SerialReadCallback,hObject,handles})
    
end