function [angleBendPlot, position1, position2] = createAnglePlot(handles)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
axes(handles.anglePlot)
angleBendPlot=plot(0,0,'b','linewidth',2);
hold on
ylim([-10 190]) 
legend('Bend angle ', 'location', 'northwest'); 
set(handles.anglePlot, 'YAxisLocation', 'right')
xlabel('Time[s]');
ylabel('Degree');

axes(handles.FrontRearPlot)
position1=plot(0,0,'b','linewidth',2);
hold on;
position2=plot(0,0,'r','linewidth',2);
hold on
ylim([-10 190]) 
legend('Front angle','Rear angle', 'location', 'northwest'); 
set(handles.FrontRearPlot, 'YAxisLocation', 'right')
xlabel('Time[s]');
ylabel('Degree');
end

