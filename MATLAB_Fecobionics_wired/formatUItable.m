function patientTableData = formatUItable(data,selected)
colergen = @(color,fontcolor,text) ['<html><table border=0 width=400 bgcolor=',color,'><TR><TD><font color="',fontcolor,'">',text,'</font></TD></TR> </table></html>'];
if ~isempty(selected)
    selectRow = selected(1);
    for i = 1:size(data,1)
        for j=1:size(data,2)
            if i==selectRow
                if isequal(data{i,j},'') %this is neede to format the cells correct when nothing is in them.
                data(i,j) = {colergen('#0000FF','#0000FF','-')}; 
                else
                data(i,j) = {colergen('#0000FF','#FFFFFF',data{i,j})};
                end
            else
                data(i,j) = {colergen('#FFFFFF','#000000',data{i,j})};
            end
        end
    end
    patientTableData = data;
elseif isnan(selected) %used when loading the table
    for i = 1:size(data,1)
        for j=1:size(data,2)
             if isequal(data{i,j},'') %this is neede to format the cells correct when nothing is in them.
                data(i,j) = {colergen('#000000','#000000','-')}; 
             else
                data(i,j) = {colergen('#FFFFFF','#000000',data{i,j})};
             end
         end
     end
end
