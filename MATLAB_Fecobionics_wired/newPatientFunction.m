function newPatientFunction(handles)
handles.patientID = num2str(handles.getPatientID);

if get(handles.radiobuttonMale, 'Value')
    radioSex = 'Male';
end

if get(handles.radiobuttonFemale, 'Value')
    radioSex = 'Female';
end

if ~get(handles.radiobuttonFemale, 'Value') && ~get(handles.radiobuttonMale, 'Value')
    radioSex = 'Not selected';
end

name = get(handles.NameText,'String');
sex = radioSex;
dob = get(handles.DOBText, 'String');
examinator = get(handles.DoctorText, 'String');
comment = get(handles.CommentsText, 'String');

if isempty(name)
    msgbox('Fill out all the requiered information', 'Error','error');
else
    patientInfo.name = name;
    patientInfo.ID = handles.patientID;
    patientInfo.sex = sex;
    patientInfo.dateOfBirth = dob;
    patientInfo.examinator = examinator;
    patientInfo.comment = comment;
    pathData = Resources('pathToExaminiations');
    pathAndFilename = [pathData handles.getPatientID '.mat'];
    save([pathAndFilename ],'patientInfo')
    close(handles.figure1);
end

