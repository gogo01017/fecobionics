function [bend, angle_1, angle_2] = BendCalc(quat1, quat2)
%   Syntax:
%   bend = BendCalc(quat1, quat2)
%
%   Version 1.0
%   By jeppe,
%   modified by FLU 25/11-2015
numbOfPackages = size(quat1,2);
if numbOfPackages == 1
    rotFlag = 1;    
    quat1 = quat1';
    quat2 = quat2';
else
    rotFlag = 1;
end
quat1 = qnorm(quat1);
quat2 = qnorm(quat2);
angle_1 = abs(quat1(1,:)*180);
angle_2 = abs(quat2(1,:)*180);
V_Front_start = [0; 0; 1];
V_Back_start = [0; 0; -1];

V_Front_2 = zeros(numbOfPackages, 3);
V_Back_2 = zeros(numbOfPackages, 3);
CosTheta_2 = zeros(numbOfPackages, 1);
ThetaInDegrees_2 = zeros(numbOfPackages, 1);

try
    if isequal(numbOfPackages,1)
        V_Front_rot = quat2rotm(quat1);
        V_Back_rot = quat2rotm(quat2);
    else
        V_Front_rot = quat2rotm(quat1');
        V_Back_rot = quat2rotm(quat2');
        
    end
catch
    disp('hasdf')
end
try
    for i = 1:numbOfPackages
        V_Front_2(i,:) = V_Front_rot(:,:,i) * V_Front_start;
        V_Back_2(i,:) = V_Back_rot(:,:,i) * V_Back_start;
    end
catch
    disp('her')
end

try
    for i = 1:numbOfPackages
        CosTheta_2(i) = dot(V_Front_2(i,:).',V_Back_2(i,:).')/(norm(V_Front_2(i,:).')*norm(V_Back_2(i,:).'));
        ThetaInDegrees_2(i) = acos(CosTheta_2(i))*180/pi;
    end
catch
    disp('haedrf')
end
bend = ThetaInDegrees_2;
