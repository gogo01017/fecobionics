function [comportString, successFlag]= findSerialPort()
%FLU. Finder den relevante comport eller sp�rger brugeren.

Skey = 'HKEY_LOCAL_MACHINE\HARDWARE\DEVICEMAP\SERIALCOMM';
% Find connected serial devices and clean up the output
[~, list] = dos(['REG QUERY ' Skey]);
list = strread(list,'%s','delimiter',' ');
coms = 0;
successFlag = 1;
if isempty(list)
        %error handled elsewhere
        comportString = 0;
        successFlag = 0;    
else
    for i = 1:numel(list)
        if strcmp(list{i}(1:3),'COM')
            if ~iscell(coms)
                coms = list(i);
            else
                coms{end+1} = list{i};
            end
        end
    end
    if length(coms)>1
        [v,~] = listdlg('PromptString','Select the Fecobionic Com port:',...
            'SelectionMode','single',...
            'ListString',coms);
        comportString= coms{v};
    elseif isequal(coms,0)
        % no port found.

    elseif length(coms)==1
        if iscell(coms)
            comportString = coms{1};
        else
            comportString = coms;
        end
    else
        comportString = 0;
        successFlag = 0;
    end
end