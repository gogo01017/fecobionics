function Export(handles,allOrSection)
    filename = strcat(handles.patientID, '.mat');
    examinFullPath = Resources('pathToExaminiations');
    
    patientData = load([examinFullPath filename]);
    patientInfo = patientData.patientInfo;
    examinationNo = handles.selectExamination;
    
    examinationData = patientData.data(examinationNo);
    dname = Resources('dataPath');
   
sectionOK = 1;
if isequal(allOrSection,'section')
 
    xlimites = get(handles.pressPlot,'xlim');
    idxStart = find(examinationData.timeX > xlimites(1));
    if isempty(idxStart)
        sectionOK = 0;
        f = msgbox('The chosen section does not contain data. Nothing is esported');
        waitfor(f)
        
    else
        idxStart = idxStart(1);
        
        idxEnd = find(examinationData.timeX < xlimites(2));
        if isempty(idxEnd)
            sectionOK = 0;
            f = msgbox('The chosen section does not contain data. Nothing is esported');
            waitfor(f)
        else
           idxEnd = idxEnd(end); 
        end
    end
   
elseif isequal(allOrSection,'all')
    idxStart = 1;
    idxEnd = length(examinationData.timeX);
end
   % try
        Startline       = 'Fecobionic measurement';
        examNo          = ['Examination number: ' examinationData.examNo];
        PatientID       = ['Patient ID: ' patientInfo.ID];
        PatientName     = ['Patient name: ' patientInfo.name];
        PatientBOD      = ['Patient date of birth: ' patientInfo.dateOfBirth];
        Examinator      = ['Examinator: ' patientInfo.examinator];
        PatientComment  = ['Patient comment: ' patientInfo.comment];
        exportTime      = ['Export time: ' datestr(now,'dd.mm.yyyy HH.MM.ss')];
        examinTime      = ['Examination time: ' examinationData.startTime];
        Volume          = ['Volume: ' examinationData.volume];
        ExportSection   = ['Exported all or section: ' allOrSection];
        Samplefrq       = ['SampleFrequence: ' num2str(examinationData.sampleFreq) 'Hz'];
        AvgFreq         = ['Average sample freqency in relation to the CPU: ',num2str(round(length(examinationData.timeX(idxStart:idxEnd))/(examinationData.timeX(idxEnd)-examinationData.timeX(idxStart)),1)),' Hz'];
        softwareVers    = 'Software version: 2.1';
        Headers         = ['Time; ' 'Temperature Front; ' 'Temperature Bag; ' 'Temperature rear; ' 'Pressure front; ' 'Pressure bag; ' 'Pressure rear; ' 'Bend angle; ' ...
            'Gyro Front1; ' 'Gyro Front2; ' 'Gyro Front3; ' 'Gyro Front4; ' 'Gyro Rear1; ' 'Gyro Rear2; ' 'Gyro Rear3; '...
            'Gyro Rear4; ' 'Gyro WTU1; ' 'Gyro WTU2; ' 'Gyro WTU3; ' 'Gyro WTU4; ' 'Angle Front1; ' 'Angle Front2; ' ...
            'Angle Front3; ' 'Angle Rear1; ' 'Angle Rear2; ' 'Angle Rear3; ' 'Angle WTU1; ' 'Angle WTU2; ' 'Angle WTU3'];
        %define filename
        Filename = [patientInfo.ID,'_Ex',examinationData.examNo,'_', datestr(now,'dd.mm.yyyy HH.MM.ss'),'_',allOrSection, '.csv'];
        FilenameAndPath=[dname Filename];
        
        
        dlmwrite(FilenameAndPath, Startline,'delimiter', '');
        dlmwrite(FilenameAndPath, examNo,'-append','delimiter', '');
        dlmwrite(FilenameAndPath, PatientID ,'-append', 'delimiter', '');
        dlmwrite(FilenameAndPath, PatientName ,'-append', 'delimiter', '');
        dlmwrite(FilenameAndPath, PatientComment ,'-append', 'delimiter', '');
        dlmwrite(FilenameAndPath, PatientBOD ,'-append', 'delimiter', '');
        dlmwrite(FilenameAndPath, Examinator ,'-append', 'delimiter', '');
        dlmwrite(FilenameAndPath, exportTime ,'-append', 'delimiter', '');
        dlmwrite(FilenameAndPath, examinTime ,'-append', 'delimiter', '');
        dlmwrite(FilenameAndPath, ExportSection ,'-append', 'delimiter', '');  
        dlmwrite(FilenameAndPath, Volume ,'-append', 'delimiter', '');
        dlmwrite(FilenameAndPath, Samplefrq ,'-append', 'delimiter', '');
        dlmwrite(FilenameAndPath, AvgFreq ,'-append', 'delimiter', '');
        dlmwrite(FilenameAndPath, softwareVers ,'-append', 'delimiter', '');
        
        dlmwrite(FilenameAndPath, 'Comments:' ,'-append', 'delimiter', '');
        commentData=examinationData.commentData.commentData;
        lengthOfData=size(examinationData.commentData.commentData);
        commentscell=cell(lengthOfData(1,1),1);
        for i=1:lengthOfData(1,1)
            commentscell{i,1}=[commentData{i,1},';',commentData{i,2}];
        end
        for i=1:lengthOfData(1,1)
            dlmwrite(FilenameAndPath,commentscell{i},'-append', 'delimiter','')
        end
        dlmwrite(FilenameAndPath, Headers ,'-append', 'delimiter', '');
        data = [examinationData.timeX(idxStart:idxEnd)' examinationData.temperature(1,idxStart:idxEnd)' examinationData.temperature(2,idxStart:idxEnd)' examinationData.temperature(3,idxStart:idxEnd)' examinationData.pressure(1,idxStart:idxEnd)' examinationData.pressure(2,idxStart:idxEnd)' ...
            examinationData.pressure(3,idxStart:idxEnd)' examinationData.angleBend(idxStart:idxEnd)' examinationData.gyroFront(1,idxStart:idxEnd)' examinationData.gyroFront(2,idxStart:idxEnd)' ...
            examinationData.gyroFront(3,idxStart:idxEnd)' examinationData.gyroFront(4,idxStart:idxEnd)' examinationData.gyroRear(1,idxStart:idxEnd)' examinationData.gyroRear(2,idxStart:idxEnd)'...
            examinationData.gyroRear(3,idxStart:idxEnd)' examinationData.gyroRear(4,idxStart:idxEnd)' examinationData.angleFront(1,idxStart:idxEnd)' examinationData.angleFront(2,idxStart:idxEnd)'...
            examinationData.angleFront(3,idxStart:idxEnd)' examinationData.angleRear(1,idxStart:idxEnd)' examinationData.angleRear(2,idxStart:idxEnd)' examinationData.angleRear(3,idxStart:idxEnd)' ...
            examinationData.angleWTU(1,idxStart:idxEnd)' examinationData.angleWTU(2,idxStart:idxEnd)' examinationData.angleWTU(3,idxStart:idxEnd)'];
        dlmwrite(FilenameAndPath, data, '-append', 'delimiter', ';')
        
        msgbox({'The data was successfully exportet.';' ';'Filename: '; Filename;' ';'Folder: ';dname}, 'Data exportet');
  %  catch
  %      msgbox('It was not possible to export the data','Data not exportet', 'error');
  %  end



