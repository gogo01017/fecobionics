function saveData(handles)
fullPathToCommentData = Resources('fullPathToCommentData');
commentData = load(fullPathToCommentData);
filename = strcat(handles.patientID, '.mat');
pathname = Resources('pathToExaminiations');

examNum = num2str(handles.examNo);
filename_xls = strcat(handles.patientID,'_',examNum, '.xls');
fileData = load([pathname,filename]);
Dpath = 'C:\Users\user\Desktop\FecoData\Excel';
 
if isfield(fileData,'data')
    num = length(fileData.data)+1;
    data = fileData.data;
    data(num).examNo = examNum;
    
    data(num).startTime = handles.startTime;
    data(num).volume     = get(handles.volumeText, 'UserData');        
    data(num).sampleFreq = handles.sampleFreq;
    data(num).commentData = commentData(:,:);
    
    data(num).timeX      = handles.timeX;
    data(num).gyroFront  = handles.gyroFront;
    data(num).gyroRear   = handles.gyroRear;
    data(num).gyroWTU    = handles.gyroWTU;
    data(num).acc_angle  = handles.acc_angle;
    data(num).gy_angle   = handles.gy_angle;
    data(num).pressure   = handles.pressure;
    
    data(num).accFront  = handles.accFront;
    data(num).accRear   = handles.accRear;
    
    data(num).temperature= handles.temperature;
    
    data(num).angleFront = handles.angleFront;
    data(num).angleRear  = handles.angleRear;
    data(num).angleWTU   = handles.angleWTU;
    data(num).angleBend  = handles.bend;
     data(num).acc_gy_angle_front = handles.acc_gy_angle1;
    data(num).acc_gy_angle_rear = handles.acc_gy_angle2;

    for cntr_files = 1 : length(data(num).timeX)
   
    Time{cntr_files} = data(num).timeX(cntr_files)';
    Pressure_1{cntr_files} = data(num).pressure(1,cntr_files)';
     Pressure_2{cntr_files} = data(num).pressure(2,cntr_files)';
     Pressure_3{cntr_files} = data(num).pressure(3,cntr_files)';
      Bend_angle{cntr_files} = data(num).angleBend(cntr_files)';
        Front_angle{cntr_files} = data(num).acc_gy_angle_front(cntr_files)';
        Rear_angle{cntr_files} = data(num).acc_gy_angle_rear(cntr_files)';
         Temperature{cntr_files} = data(num).temperature(cntr_files)';
    end
    myTime{1} = 'Time';
    myTime(2:length(Time)+1,1)= Time';
     myPressure_1{1} = 'Front Pressure';
     myPressure_2{1} = 'Bag Pressure';
      myPressure_3{1} = 'Rear Pressure';
    myBend{1} = 'Bending angle';
     myFrontAngle{1} = 'Front angle';
      myRearAngle{1} = 'Rear angle';
      myTemperature{1} = 'Temperature';
   
    myPressure_1(2:length(Pressure_1)+1) = Pressure_1;
     myPressure_2(2:length(Pressure_2)+1) = Pressure_2;
     myPressure_3(2:length(Pressure_3)+1) = Pressure_3;
     myBend(2:length(Time)+1) = Bend_angle';
     myFrontAngle(2:length(Time)+1) = Front_angle';
     myRearAngle(2:length(Time)+1) = Rear_angle';
     myTemperature(2:length(Time)+1) = Temperature';
  
%     xlswrite(filename_xls,data(num).acc_gy_angle_front);
else
    data.examNo = examNum;
    data.patient.patientID  = handles.patientID;
    data.patient.patientName  = handles.patientName;
    data.patient.patientComment = handles.patientComment;
    
    data.startTime = handles.startTime;
    data.volume     = get(handles.volumeText, 'UserData');        
    data.sampleFreq = handles.sampleFreq;
    data.commentData = commentData(:,:);
    
    data.timeX      = handles.timeX;
    data.gyroFront  = handles.gyroFront;
    data.gyroRear   = handles.gyroRear;

    data.gyroWTU    = handles.gyroWTU;
     data.accFront  = handles.accFront;
    data.accRear   = handles.accRear;
      data.acc_angle  = handles.acc_angle;
    data.gy_angle   = handles.gy_angle;
    data.pressure   = handles.pressure;
    
    data.temperature= handles.temperature;
    
    data.angleFront = handles.angleFront;
    data.angleRear  = handles.angleRear;
   
    data.angleBend  = handles.bend;
    data.acc_gy_angle_front = handles.acc_gy_angle1;
    data.acc_gy_angle_rear = handles.acc_gy_angle2;
    fileData.data = data;

       for cntr_files = 1 : length(data.timeX)
    Time{cntr_files} = data.timeX(cntr_files)';
    Pressure_1{cntr_files} = data.pressure(1,cntr_files)';
     Pressure_2{cntr_files} = data.pressure(2,cntr_files)';
     Pressure_3{cntr_files} = data.pressure(3,cntr_files)';
      Bend_angle{cntr_files} = data.angleBend(cntr_files)';
        Front_angle{cntr_files} = data.acc_gy_angle_front(cntr_files)';
        Rear_angle{cntr_files} = data.acc_gy_angle_rear(cntr_files)';
         Temperature{cntr_files} = data.temperature(cntr_files)';
       end
    
    myTime{1} = 'Time';
    myTime(2:length(Time)+1,1)= Time';
    myPressure_1{1} = 'Front Pressure';
     myPressure_2{1} = 'Bag Pressure';
      myPressure_3{1} = 'Rear Pressure';
    myBend{1} = 'Bending angle';
     myFrontAngle{1} = 'Front angle';
      myRearAngle{1} = 'Rear angle';
      myTemperature{1} = 'Temperature';
    myPressure_1(2:length(Pressure_1)+1) = Pressure_1;
     myPressure_2(2:length(Pressure_2)+1) = Pressure_2;
     myPressure_3(2:length(Pressure_3)+1) = Pressure_3;
     myBend(2:length(Time)+1) = Bend_angle';
     myFrontAngle(2:length(Time)+1) = Front_angle';
     myRearAngle(2:length(Time)+1) = Rear_angle';
     myTemperature(2:length(Time)+1) = Temperature';
 %   xlswrite(filename_xls,data.acc_gy_angle_front);
end
pathToExData = Resources('pathToExaminiations');
patientInfo = fileData.patientInfo;


save([pathname,filename], 'data','patientInfo')
  dataTable = table(myTime,myPressure_1',myPressure_2',myPressure_3',myBend',myFrontAngle',myRearAngle',myTemperature');
    cd(Dpath);
    writetable(dataTable,filename_xls,'Sheet',1);
f = msgbox({'Patient datafile is update with the new data:';' ';[pathToExData filename]});
uiwait(f);