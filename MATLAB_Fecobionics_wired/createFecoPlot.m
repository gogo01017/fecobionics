function [color, surfHandle1, surfHandle2, surfHandle3] = createFecoPlot(handles)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
axes(handles.fecoPlot)
minColor=-10;
maxColor=120;
R=[0 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 3 0];
[X,Y,Z] = cylinder(R);
rr=[0 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 0];
[xx,yy,zz]=cylinder(rr);
% balloon 
Z = Z * 10+1;

%lower core
zz1= zz * 6;

%upper core
zz2=zz*6+6;
color = colormap(handles.fecoPlot, hsv(130));
surfHandle1 = surf(xx,yy,zz2, 'facecolor',[0.4 0.4 0.4]);
hold on
surfHandle2 = surf(xx,yy,zz1,'facecolor',[0.4 0.4 0.4]);
surfHandle3 = surf(X,Y,Z,'facecolor',[0.4 0.4 0.4]);
axis off
grid off
%axis equal
%title('Fecobionic')
caxis([minColor maxColor]);
hColorbar = colorbar; 
ylabel(hColorbar, 'cm H_2O');
hold off
view([0 0]);
end

