%addpath(genpath('C:\Users\GIOME\Documents\MATLAB\'))
try
   
p = mfilename;
fullPath = mfilename('fullpath');
pathToAdd = strrep(fullPath,p,'');
addpath(genpath(pathToAdd));

    %check if the temp and examination folder exists
    tempPath = Resources('pathToTemp');
    createPath = 0;
    if ~isdir(tempPath)
        createPath = 1;
        mkdir(tempPath)
        f = msgbox({'The folder for the temxpoarry files was not found and therefore created.';' ';'The folder is placed here:';tempPath},'Created Folder');
        waitfor(f);
        createPath = 2;
    end
    
    exPath = Resources('pathToExaminiations');
    createExPath = 0;
    if ~isdir(exPath)
        createExPath = 1;
        mkdir(exPath)
        f= msgbox({'The folder for the examination files was not found and therefore created.';' ';'The folder is placed here:';tempPath},'Created Folder');
        waitfor(f);
        createExPath = 2;
    end
    
    Info
    
catch
    if createPath
        f = msgbox({'Cannot start the Fecobionice software. Could not create the temporary folder at:'; tempPath;'Please contact GIOME inc.';'The program will now exit.'},'Unable to create folder');
        waitfor(f);
    end
    if createExPath
        f= msgbox({'Cannot start the Fecobionice software. Could not create the examination folder at:'; exPath;'Please contact GIOME inc.';'The program will now exit.'},'Unable to create folder');
        waitfor(f)
    end
    
    if ~createPath || ~createExPath
        f = msgbox({'Uncaught exception.';' ';'Please contact GIOME inc.';'The program will now exit.'},'Undefined error');
        waitfor(f)
    end
   quit
end