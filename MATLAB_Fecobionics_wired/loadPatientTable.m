function [handles] = loadPatientTable(handles)
% % %UNTITLED3 Summary of this function goes here
% % %   Detailed explanation goes here
    pathToTemp = Resources('pathToExaminiations');  
        exFiles = dir([pathToTemp,'*.mat']);
        for i = 1:length(exFiles)
            p = load([pathToTemp exFiles(i).name]);
             patientInformation(i,1) = {p.patientInfo.name};
             patientInformation(i,2) = {p.patientInfo.ID};
             if isfield(p,'data')
                 patientInformation(i,3) = {num2str(length(p.data))}; %number of examinations
                 date = p.data(end).startTime;
                 patientInformation(i,4) = {date(1:end)};
             else
                 patientInformation(i,3) = {''};
                 patientInformation(i,4) = {''};
             end
             if size(p.patientInfo.comment,1)<2
                 patientInformation(i,5) = {p.patientInfo.comment};
             else
                 if size(p.patientInfo.comment(1,:),2)>40
                 patientInformation(i,5) = {[p.patientInfo.comment(1,1:40) '...']};
                 else
                     patientInformation(i,5) = {p.patientInfo.comment(1,:)};
                 end
             end
        end 
     if length(exFiles)>0
     formatedPatientTableData = formatUItable(patientInformation,[nan]);
     handles.patientTableData = patientInformation;
     set(handles.patientTable,'Data',formatedPatientTableData);
     end
 end

% % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % %Old file with database
% % load('patientData');
% % handles.patientName = patientData(:,1);
% % handles.patientID = patientData(:,2);
% % handles.patientComment = patientData(:,6);
% % 
% % for i=1:length(handles.patientName)
% %     patientInformation(i,1) = handles.patientName(i);
% %     patientInformation(i,2) = handles.patientID(i);
% %     patientInformation(i,5) = handles.patientComment(i);
% %     filename = strcat(handles.patientID, '.mat');
% %     
% %     pathToExaminiations =  Resources('pathToExaminiations');
% %     if exist([pathToExaminiations filename{i}], 'file')
% %         files = load([pathToExaminiations filename{i}]);
% %             patientInformation(i,3) = {num2str(length(files.data))};
% %             date = files.data(end).startTime;
% %             patientInformation(i,4) = {date(1:end)};
% %         end
% %     end
% %     
% %     formatedPatientTableData = formatUItable(patientInformation,[nan]);
% %     handles.patientTableData = patientInformation;
% %     set(handles.patientTable,'Data',formatedPatientTableData);
% % end
% % 
% % 
