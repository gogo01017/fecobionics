function [fecoAnglePlot] = createFecoAngle(handles)
% Syntax:
% fecoAnglePlot = createFecoAngle(handles)
%
% FLU: 26/11-2015
% Version 1.0. 

axes(handles.fecoAngle)

fecoAnglePlot = plot(nan,nan,'-or','linewidth',5,'markersize',30);

axis off;
xlim([-6 6])
ylim([-6 6])




