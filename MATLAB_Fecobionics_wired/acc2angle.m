function [acc_angle, acc_angle_x1, acc_angle_y1, acc_angle_z1, acc1_angle, acc2_angle,theta]  = acc2angle( acc1 , acc2 ,avgAcc, avgAcc1, avgAcc_h)
  acc1(1,:) = acc1(1,:)-avgAcc_h(1);
  acc1(2,:) = acc1(2,:)-(avgAcc(2)+avgAcc1(2))/2;
  acc1(3,:) = acc1(3,:)-(avgAcc(3)+avgAcc1(3))/2;
% 
  acc2(1,:) = acc2(1,:)-avgAcc_h(4);
  acc2(2,:) = acc2(2,:)-(avgAcc(5)+avgAcc1(5))/2;
  acc2(3,:) = -acc2(3,:)+(avgAcc(6)+avgAcc1(6))/2;
numbOfPackages = size(acc1,2);
cos_angle = zeros(numbOfPackages,1);
acc_angle = zeros(numbOfPackages,1);
cos_acc1_angle = zeros(numbOfPackages,1);
cos_acc2_angle = zeros(numbOfPackages,1);
acc1_angle = zeros(numbOfPackages,1);
acc2_angle = zeros(numbOfPackages,1);
acc_angle_x1 = zeros(numbOfPackages,1);
acc_angle_y1 = zeros(numbOfPackages,1);
acc_angle_z1 = zeros(numbOfPackages,1);
acc_angle_x2 = zeros(numbOfPackages,1);
cos_theta = zeros(numbOfPackages,1);

theta = zeros(numbOfPackages,1);

%  for i = 1:numbOfPackages
%        cos_angle(i) = (-acc1(1,i) * acc2(1,i) + acc1(2,i)*acc2(2,i)) / (sqrt(acc1(1,i)^2 + acc1(2,i)^2) * sqrt(acc2(1,i)^2 + acc2(2,i)^2));
%            acc_angle(i) = acos(cos_angle(i))*180/pi;
%  end

for i = 1:numbOfPackages
%     cos_angle(i) = (-acc1(1,i) * acc2(1,i) + acc1(3,i)*acc2(3,i) )/ (sqrt(acc1(1,i)^2 + acc1(3,i)^2) * sqrt(acc2(1,i)^2 + acc2(3,i)^2));
%    acc_angle(i) =180- acos(cos_angle(i))*180/pi;
%% Angle calculated by vector
cos_theta(i) = dot(acc1(1:3,i),acc2(1:3,i))/(norm(acc1(1:3,i))*norm(acc2(1:3,i)));
theta(i) = acos(cos_theta(i))*180/pi;
%%
%    if acc2(1,i)>17000
%      acc2(1,i) = 17000;
%    end
%    if acc2(1,i) <-15000
%      acc2(1,i) = -15000;
%    end

  if acc1(1,i) >= 0
      if avgAcc(1)>0
          if acc1(1,i)>avgAcc(1)
             acc1(1,i)=avgAcc(1);
         end
     acc_angle_x1(i) = asin(acc1(1,i) / avgAcc(1))*180/pi;
      else
          if acc1(1,i)>avgAcc(4)
             acc1(1,i)=avgAcc(4);
          end
      acc_angle_x1(i) = asin(acc1(1,i) / avgAcc(4))*180/pi;
      end
  end
    if acc1(1,i) < 0
         if avgAcc(1)<0
               if acc1(1,i)<avgAcc(1)
             acc1(1,i)=avgAcc(1);
               end
     acc_angle_x1(i) = asin(acc1(1,i) / -avgAcc(1))*180/pi;
               else
             if acc1(1,i)<avgAcc(4)
             acc1(1,i)=avgAcc(4);
             end
              acc_angle_x1(i) = asin(acc1(1,i) / -avgAcc(4))*180/pi;
         end  
               end
         
     acc_angle_y1(i) = asin(acc1(2,i) / 16384)*180/pi;
     acc_angle_z1(i) = asin(acc1(3,i) / 16384)*180/pi ;  
%        if acc2(1,i) >= 0
%       acc_angle_x2(i) = asin(acc2(1,i) / 17000)*180/pi;
%        end
%            if acc2(1,i) < 0
%        acc_angle_x2(i) = asin(acc2(1,i) / 15000)*180/pi;
%            end
 %%          
%    if acc1(1,i) >16384
%       acc1(1,i) = 16384;
%   end
%    if acc1(1,i) <-16384
%       acc1(1,i)= -16384;
%    end

%   acc_angle_x1(i) = asin(acc1(1,i) / 16384)*180/pi;
if acc2(1,i)>0
     if avgAcc1(1)>0
         if acc2(1,i)>avgAcc1(1)
             acc2(1,i)=avgAcc1(1);
         end
     acc_angle_x2(i) = asin(acc2(1,i) / avgAcc1(1))*180/pi;
     else
          if acc2(1,i)>avgAcc1(4)
             acc2(1,i)=avgAcc1(4);
         end
      acc_angle_x2(i) = asin(acc2(1,i) / avgAcc1(4))*180/pi;
      end 
end
if acc2(1,i)<0
     if avgAcc1(1)<0
          if acc2(1,i)<avgAcc1(1)
             acc2(1,i)=avgAcc1(1);
         end
     acc_angle_x2(i) = asin(acc2(1,i) / -avgAcc1(1))*180/pi;
     else
           if acc2(1,i)<avgAcc1(4)
             acc2(1,i)=avgAcc1(4);
         end
      acc_angle_x2(i) = asin(acc2(1,i) / -avgAcc1(4))*180/pi;
      end 
end
%%           
%     cos_acc1_angle(i) = (-acc1(1,i) * avgAcc(1)-acc1(2,i) * avgAcc(2)  + acc1(3,i)* avgAcc(3)) / (sqrt(acc1(1,i)^2 + acc1(2,i)^2 + acc1(3,i)^2)  * sqrt(avgAcc(1)^2  + avgAcc(2)^2+ avgAcc(3)^2));
 %   acc1_angle(i) =180- acos(cos_acc1_angle(i))*180/pi;  
acc1_angle(i) = 90 + acc_angle_x1(i);
%     cos_acc2_angle(i) = (-acc2(1,i) * avgAcc(4)-acc2(2,i) * avgAcc(5) + acc2(3,i)* avgAcc(6)) / (sqrt(acc2(1,i)^2 + acc2(2,i)^2+ acc2(3,i)^2)  * sqrt(avgAcc(4)^2 + avgAcc(5)^2+ avgAcc(6)^2));
%    acc2_angle(i) =180- acos(cos_acc2_angle(i))*180/pi;  
acc2_angle(i) = 90 - acc_angle_x2(i);
acc_angle(i) =180- abs( acc1_angle(i) - acc2_angle(i));
end
% 
%   for i = 1:numbOfPackages
%         cos_angle(i) = (-acc1(1,i) * acc2(1,i)- acc1(2,i)*acc2(2,i) + acc1(3,i)*acc2(3,i)) / (sqrt(acc1(1,i)^2 + acc1(2,i)^2+ acc1(3,i)^2)  * sqrt(acc2(1,i)^2+ acc2(2,i)^2 + acc2(3,i)^2));
%             acc_angle(i) =180- acos(cos_angle(i))*180/pi;
%   end

