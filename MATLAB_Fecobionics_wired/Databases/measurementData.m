%Patient datbase  -----------------------------------------
time = {''};
pressureFront = {''};
pressureBack = {''};
pressureBag = {''};
geometries1 = {''};
geometries2 = {''};
geometries3 = {''};
geometries4 = {''};
geometries5 = {''};
geometries6 = {''};
geometries7 = {''};
geometries8 = {''};
gyrocopeFrontX = {''};
gyrocopeFrontY = {''};
gyrocopeFrontZ = {''};
gyrocopeBackX = {''};
gyrocopeBackY = {''};
gyrocopeBackZ = {''};
vector1 = {''};
vector2 = {''};
angle = {''};
volume = {''};
temperature = {''};
comment = {''};

measuremtData = [time pressureFront pressureBack pressureBag geometries1 geometries2 geometries3 geometries4 geometries5
    geometries6 geometries7 geometries8 gyrocopeFrontX gyrocopeFrontY gyrocopeFrontZ gyrocopeBackX gyrocopeBackY gyrocopeBackZ
    vector1 vector2 angle volume temperature comment];

save('measuremtData', 'measuremtData', '-v7.3');