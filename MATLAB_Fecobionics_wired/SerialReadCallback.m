function SerialReadCallback(serialobj, unused, hObject, handles)
% syntax:
%     SerialReadCallback(varargin)
%
% Varargin is: 
handles=guidata(hObject);
global bend;
global acc_angle;
global acc_gy_angle;
global acc_gy_angle_1;
global theta;
%global gy_angle;
global acc_gy_angle1;
global acc_gy_angle2;
handles.com.dataOffsets.OFFSET_CHECKSUM_HB		= 98; %98
handles.com.dataOffsets.OFFSET_CHECKSUM_LB		= 99; %99

dataPacketReceived = 0;
numOfDataPackages = 0;
blockSize = 99;
dataPacket = zeros(blockSize, 1);
bytesAvail = serialobj.BytesAvailable;
pause(0.05);

%der skal kunne l�ses minimum en pakke ud
if bytesAvail > 72
%try    
    %tager x antal pakker ud af gangen
    handles.missingdataCount = 0;
    numOfDataPackages = floor(serialobj.BytesAvailable/99);
    [intvalues,~,~]=fread(serialobj,numOfDataPackages*99,'uint8');
    
    %reshaped in packages
    intvalues = reshape(intvalues,99,numOfDataPackages);
    
    %If each datapackage is not starting with a AB (171) value it must
    %reinitialize (not made yet)
    if sum(intvalues(1,:)) == 171*numOfDataPackages
        
        %start data CheckSum calculations
        validatedPackages = 0;
        corruptPackages = [];
        for i = 1:numOfDataPackages
            CheckSumCalc = CheckSum(intvalues(:,i), blockSize-2);
            CheckSumBuf = [intvalues(handles.com.dataOffsets.OFFSET_CHECKSUM_LB,i) intvalues(handles.com.dataOffsets.OFFSET_CHECKSUM_HB,i)];
            CheckSumMsg = typecast(uint8(CheckSumBuf), 'UINT16');
            
            if (CheckSumCalc == CheckSumMsg)
                switch intvalues(3,i);
                    case 5
                        validatedPackages = validatedPackages + 1;
                    case 20
                        stopCommand();
                        msgbox('The connection is lost. The examination is therefor stoped', 'error', 'error')
                    case 21 || 22 || 23 || 24 || 25 || 26
                        corruptPackages  = [corruptPackages i];
                        error = intvalues(3,i);
                        f = Error(error);
                        waitfor(f);
                end
            else
                corruptPackages  = [corruptPackages i];
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %Not taken care of corruptPackages
        if ~isempty(corruptPackages)
            f = msgbox('Corupt pacakages was found. This is an message in relation to tests.', 'error', 'error');
            uiwait(f)
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %starts the data extraction
        if validatedPackages == numOfDataPackages
            
            [timeSample,gyro,gyroSampleTime,pressure,temperature,acc] = unpackData(intvalues);
            pressure = calibrateAndConvertData(pressure,handles.avgPress);
            handles.timeX = [handles.timeX timeSample];
            handles.gyroFront = [handles.gyroFront gyro(1:4,:)];
            handles.gyroRear = [handles.gyroRear gyro(5:8,:)];
%            handles.gyroWTU = [handles.gyroWTU gyro(9:12,:)];
            handles.pressure = [handles.pressure pressure];
            handles.accFront = [handles.accFront acc(1:3,:)];
            handles.accRear = [handles.accRear acc(4:6,:)];
            %temperature
            handles.temperature = [handles.temperature temperature];
            
            [angles] = convertToAngle(gyro);
            
            handles.angleFront = [handles.angleFront angles(1:3,:)];
            handles.angleRear = [handles.angleRear angles(4:6,:)];
%            handles.angleWTU = [handles.angleWTU angles(7:9,:)];
          
%            if size(gyro,2)==4

%                 bend = nan(4,1);
%                 for j = 1:4
%                      bend(j) = BendCalc(gyro(1:4,j),gyro(5:8,j));    
%                 end
%             else
%                  [bend,angle_1,angle_2] = BendCalc(gyro(1:4,:),gyro(5:8,:));    
%            end
           
              
[bend,angle_1,angle_2] = BendCalc(gyro(1:4,:),gyro(5:8,:));    
            

          
          if (isempty(acc_gy_angle) ==1)
              acc_gy_angle = 180 * ones(length(bend),1);
          end
        
           acc_gy_angle(1) = acc_gy_angle(length(acc_gy_angle));
           acc_gy_angle(2:length(acc_gy_angle)) = [];
           
          if (isempty(acc_gy_angle_1) ==1)
              acc_gy_angle_1 = 180 * ones(length(bend),1);
          end
           acc_gy_angle_1(1) = acc_gy_angle_1(length(acc_gy_angle_1));
           acc_gy_angle_1(2:length(acc_gy_angle_1)) = [];
           
          if (isempty(acc_gy_angle1) ==1)
              acc_gy_angle1 = 180 * ones(length(bend),1);
          end
           acc_gy_angle1(1) = acc_gy_angle1(length(acc_gy_angle1));
           acc_gy_angle1(2:length(acc_gy_angle1)) = [];
            
          if (isempty(acc_gy_angle2) ==1)
              acc_gy_angle2 = 180 * ones(length(bend),1);
          end
           acc_gy_angle2(1) = acc_gy_angle2(length(acc_gy_angle2));
           acc_gy_angle2(2:length(acc_gy_angle2)) = [];
        
          
         [acc_angle, acc_angle_x1, acc_angle_y1, acc_angle_z1, acc1_angle, acc2_angle,theta]  = acc2angle( acc(1:3,:) , acc(4:6,:),handles.avgAcc,handles.avgAcc1,handles.avgAcc_h ) ;
%          gy_angle = gy2angle(gyro(1:4,:),gyro(5:8,:),timeSample);
          
       for i = 1:length(bend)-1
          acc_gy_angle(i+1) = 0.9*(acc_gy_angle(i) + bend(i+1) - bend(i)) + 0.1*acc_angle(i);
          acc_gy_angle1(i+1) = 0.9*(acc_gy_angle1(i) + angle_1(i+1) - angle_1(i)) + 0.1*acc1_angle(i);
           acc_gy_angle2(i+1) = 0.9*(acc_gy_angle2(i) + angle_2(i+1) - angle_2(i)) + 0.1*acc2_angle(i);
           acc_gy_angle_1(i+1) = 0.9*(acc_gy_angle_1(i) + bend(i+1) - bend(i)) + 0.1*theta(i);
           if (acc_gy_angle(i+1)>180)
              acc_gy_angle(i+1) = 180;
          end
       
         
          if (acc_gy_angle_1(i+1)>180)
              acc_gy_angle_1(i+1) = 180;
          end
       end
        
       
            handles.acc_gy_angle1 = [handles.acc_gy_angle1 acc_gy_angle1];
            handles.acc_gy_angle2 = [handles.acc_gy_angle2 acc_gy_angle2];
            handles.bend_old = [handles.bend_old bend'];
            handles.bend = [handles.bend acc_gy_angle_1];
            handles.acc_angle = [handles.acc_angle acc_angle'];
            handles.gy_angle = [handles.gy_angle bend'];
            handles.acc_angle_x1 = [handles.acc_angle_x1 acc_angle_x1'];
            handles.acc_angle_y1 = [handles.acc_angle_y1 acc_angle_y1'];
            handles.acc_angle_z1 = [handles.acc_angle_z1 acc_angle_z1'];
            handles.acc1_angle = [handles.acc1_angle acc1_angle'];
            handles.acc2_angle = [handles.acc2_angle acc2_angle'];
            handles.angle_1 = [handles.angle_1 angle_1];
            handles.angle_2 = [handles.angle_2 angle_2];
            %bend
         
           pause(0.01);
            guidata(hObject, handles);
            updateGraph(hObject, handles);
        else
            disp('Error package, line 98, serial call back')
            %msgbox('The connection is lost. The examination is therefor stoped', 'error', 'error')
            %stopCommand();
            
            %must reinitialize, e.g. device stopped.
        end    
    end
else
%catch
    disp('No data');
    pause(0.1) %to enable buffer to fill up and free up some ressources for GUI
    handles.missingdataCount = handles.missingdataCount + 1;
    if(handles.missingdataCount > 15)
        fclose(serialobj);
        [serialobjNew, ~] = createSerialPort;
        serialobj = serialobjNew;
        startCommand(serialobj);
        [serialobj, successFlag] = initializeSerial(serialobj);
        pause(3)
        handles.missingdataCount = 0;
    end
    
    guidata(hObject, handles);
    
end

% % 1	Preamble
% % 2	Address
% % 3	Command
% % 4	Time
% % 5	Time
% % 6	Time
% % 7	Time
% % 8	Gyro 1, Gyro
% % 9	Gyro 1, Gyro
% % 10	Gyro 1, Gyro
% % 11	Gyro 1, Gyro
% % 12	Gyro 1, Gyro
% % 13	Gyro 1, Gyro
% % 14	Gyro 1, Gyro
% % 15	Gyro 1, Gyro
% % 16	Gyro 1, Gyro
% % 17	Gyro 1, Gyro
% % 18	Gyro 1, Gyro
% % 19	Gyro 1, Gyro
% % 20	Gyro 1, Gyro
% % 21	Gyro 1, Gyro
% % 22	Gyro 1, Gyro
% % 23	Gyro 1, Gyro
% % 24	Gyro 1, Acc
% % 25	Gyro 1, Acc
% % 26	Gyro 1, Acc
% % 27	Gyro 1, Acc
% % 28	Gyro 1, Acc
% % 29	Gyro 1, Acc
% % 30	Gyro 1, time
% % 31	Gyro 1, time
% % 32	Gyro 1, time
% % 33	Gyro 1, time
% % 34	Gyro 2, Gyro
% % 35	Gyro 2, Gyro
% % 36	Gyro 2, Gyro
% % 37	Gyro 2, Gyro
% % 38	Gyro 2, Gyro
% % 39	Gyro 2, Gyro
% % 40	Gyro 2, Gyro
% % 41	Gyro 2, Gyro
% % 42	Gyro 2, Gyro
% % 43	Gyro 2, Gyro
% % 44	Gyro 2, Gyro
% % 45	Gyro 2, Gyro
% % 46	Gyro 2, Gyro
% % 47	Gyro 2, Gyro
% % 48	Gyro 2, Gyro
% % 49	Gyro 2, Gyro
% % 50	Gyro 2, Acc
% % 51	Gyro 2, Acc
% % 52	Gyro 2, Acc
% % 53	Gyro 2, Acc
% % 54	Gyro 2, Acc
% % 55	Gyro 2, Acc
% % 56	Gyro 2, time
% % 57	Gyro 2, time
% % 58	Gyro 2, time
% % 59	Gyro 2, time
% % 60	Gyro 3, Gyro
% % 61	Gyro 3, Gyro
% % 62	Gyro 3, Gyro
% % 63	Gyro 3, Gyro
% % 64	Gyro 3, Gyro
% % 65	Gyro 3, Gyro
% % 66	Gyro 3, Gyro
% % 67	Gyro 3, Gyro
% % 68	Gyro 3, Gyro
% % 69	Gyro 3, Gyro
% % 70	Gyro 3, Gyro
% % 71	Gyro 3, Gyro
% % 72	Gyro 3, Gyro
% % 73	Gyro 3, Gyro
% % 74	Gyro 3, Gyro
% % 75	Gyro 3, Gyro
% % 76	Gyro 3, Acc
% % 77	Gyro 3, Acc
% % 78	Gyro 3, Acc
% % 79	Gyro 3, Acc
% % 80	Gyro 3, Acc
% % 81	Gyro 3, Acc
% % 82	Gyro 3, time
% % 83	Gyro 3, time
% % 84	Gyro 3, time
% % 85	Gyro 3, time
% % 86	Pressure 1
% % 87	Pressure 1
% % 88	Pressure 2
% % 89	Pressure 2
% % 90	Pressure 3
% % 91	Pressure 3
% % 92	Temp 1
% % 93	Temp 1
% % 94	Temp 2
% % 95	Temp 2
% % 96	Temp 3
% % 97	Temp 3
% % 98	Chk sum
% % 99	Chk sum
