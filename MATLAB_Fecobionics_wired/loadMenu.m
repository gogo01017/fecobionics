function loadMenu(handles)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
pathToExaminiations = Resources('pathToExaminiations');
filename = strcat(handles.patientID, '.mat');
oldExamin = load([pathToExaminiations filename]);
examinations = {'' oldExamin.data.examNo};
set(handles.examinMenu, 'String', examinations);
end

