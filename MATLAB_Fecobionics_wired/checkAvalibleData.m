function checkAvalibleData(handles)
if ~isempty(handles.selectedpatientid)
    filename = strcat(handles.selectedpatientid, '.mat');
    pathToExaminiations = Resources('pathToExaminiations');
    p = load([pathToExaminiations filename]);
    
    if ~isfield(p,'data')
        set(handles.anaysisButton, 'enable','on')
        set(handles.anaysisButton, 'enable','off')
        set(handles.startButton, 'enable','on')
    else
        set(handles.anaysisButton, 'enable','on')
        set(handles.anaysisButton, 'enable','on')
        set(handles.startButton, 'enable','on')
    end
else
disp('none selected')
end
