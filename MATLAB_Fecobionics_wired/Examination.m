function varargout = Examination(varargin)

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Results_OpeningFcn, ...
    'gui_OutputFcn',  @Results_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before Results is made visible.
function Results_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
handles.output = hObject;
movegui(gcf,'center');
%Resources('updateExamnNr');

set(handles.KeypressText,'Visible','Off'); %Comment text
degree = sprintf('%c', char(176));
set(handles.celText, 'String', [degree ' Celcius']);
handles.windowCheck = 'examination';

%S�tter patient id og navn
handles.patientID = varargin{1};
set(handles.patientIDText, 'String', handles.patientID);
handles.patientName = varargin{2};
set(handles.namePatText, 'String', handles.patientName);
handles.examNo = varargin{3};
set(handles.examNoText, 'String', handles.examNo);
handles.avgPress = varargin{4};
handles.serialobj = varargin{5};
handles.patientComment = varargin{6};
handles.avgAcc = varargin{7};
handles.avgAcc1 = varargin{8};
handles.avgAcc_h = varargin{9};
handles.startTime = datestr(now, 'dd.mm.yyyy HH.MM');
handles.volume = get(handles.volumeText, 'String');

handles.sampleFreq = 28; %%OBS RET DETTE SENERE!!!
handles = setupValues(handles);

%Create plots
[pressureballonplot, pressurefrontplot, pressurebackplot] = createPressurePlot(handles);
handles.pressureballonplot = pressureballonplot;
handles.pressurefrontplot = pressurefrontplot;
handles.pressurebackplot = pressurebackplot;

[color, surfHandle1, surfHandle2, surfHandle3] = createFecoPlot(handles);
handles.color = color;
handles.surfHandle1 = surfHandle1;
handles.surfHandle2 = surfHandle2;
handles.surfHandle3 = surfHandle3;

[angleBendPlot, position1, position2] = createAnglePlot(handles);
handles.angleBend = angleBendPlot;
handles.position1 = position1;
handles.position2 = position2;
[fecoAnglePlot] = createFecoAngle(handles);
handles.fecoAnglePlot = fecoAnglePlot;

handles.transfer = Resources('getTransferFunction');

%opdatere handles
guidata(hObject, handles);

%starter m�lingen
[serialobj, successFlag] = initializeSerial(handles.serialobj);
handles.serialobj = serialobj;
startReading(handles.serialobj, handles, hObject);

handles= guidata(hObject);
guidata(hObject, handles);
handles.avgPress;
handles.avgAcc;
handles.avgAcc1;

% --- Outputs from this function are returned to the command line.
function varargout = Results_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles.output;

% --- Executes on button press in returnButton.
function returnButton_Callback(hObject, eventdata, handles)
if strcmp(get(handles.stopButton, 'enable'), 'on')
    saveData(handles);
    stopCommand(handles);
    fclose(handles.serialobj);
    delete(handles.serialobj);
end

Info(handles.patientID, handles.patientName); %Opning patient gui
delete(get(hObject, 'parent')); %Closing this window

function stopButton_Callback(hObject, eventdata, handles)
handles.output = stopExamination();
handles.volume = get(handles.volumeText, 'String');
if strcmp(handles.output, 'Yes')
    stopCommand(handles.serialobj); %stopper kontakten til COMPort (FLU; og gemme data!)
    fclose(handles.serialobj);
    delete(handles.serialobj);
    saveData(handles);
    set(handles.CommentesButton, 'enable', 'off');
    set(handles.stopButton, 'enable', 'off');
    set(handles.headerExamin,'BackgroundColor',[1,1,1]);
    set(handles.headerExamin, 'String', 'Examination');
end

function toggelButton_Callback(hObject,eventdata,handles)

function CommentesButton_Callback(hObject,eventdata,handles)
set(handles.serialobj,'BytesAvailableFcn','');
f = PatientInput(handles.patientID, [1], handles.windowCheck);
uiwait(f);
set(handles.serialobj,'BytesAvailableFcn',{@SerialReadCallback,hObject,handles});

function Keypress(hObject, eventdata, handles)
key = eventdata.Key;
Comment(key, handles, hObject);

% function figure1_CloseRequestFcn(hObject, eventdata, handles)
% if strcmp(get(handles.stopButton, 'enable'), 'on')
%     saveData(handles);
%     stopCommand(handles);
%     fclose(handles.serialobj);
%     delete(handles.serialobj);
% end
% delete(hObject);

% Menuer--------------------------------------------------------------------
function Menu_Callback(hObject, eventdata, handles)

function AboutHead_Callback(hObject, eventdata, handles)

function About_Callback(hObject, eventdata, handles)
About();

function Exit_Callback(hObject, eventdata, handles)
closeProgram();