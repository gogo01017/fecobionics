function [serialobj, successFlag] = createSerialPort()
serialobj = 0;
%FLU; sikre at diverse porte er lukket inden vi fors�ger noget
try 
    fclose(instrfind);
catch
end

%FLU; sikre at diverse porte er lukket inden vi fors�ger noget
[serialPort, successFlag] = findSerialPort;

%COM port 
%Parity is none, parity checking is not performed and the parity bit is not transmitted
%BaudRate er pr default 9600 specify the rate at which bits are transmitted
%Databits = 8  (data is transmitted as a serial of 8 bits- binary data) default value
%StopBits = 1 (one data bit is used to indicate the end of data transmission) default value
%Inputbuffersize = the size of input buffer in bytes
%outputbuffersize = the size of output buffer in bytes
if successFlag
    InputBufferSize = 1000000;
    OutputBufferSize = 5000;
    serialobj= serial(serialPort,'Parity','none', 'Databits',8,'StopBits',1);
    set(serialobj,'BaudRate',115200) % bit/sek = hastighed gammel= 115200
    set(serialobj,'InputBufferSize',InputBufferSize)
    set(serialobj,'outputBufferSize',OutputBufferSize)
    set(serialobj,'ReadAsyncMode','Continuous')%L�ser i asynkront mode
    set(serialobj,'BytesAvailableFcnMode','byte')%funktion der bliver kaldt n�r det er bytes tilstede
    set(serialobj,'BytesAvailableFcnCount',10)
    try
        fopen(serialobj);
    catch
        successFlag = 0;
    end
end