function varargout = Setup(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Setup_OpeningFcn, ...
    'gui_OutputFcn',  @Setup_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% --- Executes just before Setup is made visible.
function Setup_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
movegui(gcf,'center');

if ~isempty(varargin)
    handles.patientID = varargin{1};
    handles.patientName = varargin{2};
    handles.patientComment = varargin{3};
else %just for testing purposes 
    handles.patientID = 'test';
    handles.patientName = 'test';
end
axes(handles.axes1);
imshow('setup.png')
axes(handles.axes2);
imshow('connections.png')

set(handles.returnButton, 'enable', 'off');
set(handles.CalibrateButton, 'enable', 'off');
set(handles.CalibrateButton1, 'enable', 'off');
set(handles.CalibrateButton2, 'enable', 'off');
set(handles.timerText,'visible','on')
set(handles.timerText,'String','Autodiscover the serial port. Please wait.');

%Opretter comport forbindelse
[handles.serialobj, successFlag] = setupAndInitializeSerial();

if successFlag
    set(handles.FecoWTUConn,'BackgroundColor',[0,1,0]);
    set(handles.WTUUCConn,'BackgroundColor',[0,1,0]);
    set(handles.timerText,'visible','off')
    set(handles.setupCommButton,'enable','off')
    set(handles.CalibrateButton, 'enable', 'on');
    set(handles.CalibrateButton1, 'enable', 'on');
       set(handles.CalibrateButton2, 'enable', 'on');
else
    set(handles.FecoWTUConn,'BackgroundColor',[1,0,0]);
    set(handles.WTUUCConn,'BackgroundColor',[1,0,0]);
    set(handles.timerText,'visible','off');
    set(handles.setupCommButton,'enable','on');
end

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = Setup_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles.output;

function returnButton_Callback(hObject, eventdata, handles)
Info(handles.patientID, handles.patientName);
delete(get(hObject, 'parent')); %Closing this window

function CalibrateButton3_Callback(hObject, eventdata, handles)
set(handles.timerText,'visible','on')
handles = setupValues(handles);
calibrationComplete = 0;
set(handles.timerText,'string','Autocalibrate, keep it still on the table')
pause(3);
numberOfTries = 1;
handles.avgPress = [0 0 0]';
handles.avgAcc_h= [0 0 0 0 0 0]';
while ~calibrationComplete
     
    [avgPress, avgAcc_h, gyroCaliFlag] = autoCalibrateProbe(handles.serialobj);
    numberOfTries = 1+numberOfTries;


    if (length(avgPress)<3)
        set(handles.timerText,'String',['Something went wrong. We try again: ',num2str(numberOfTries),' of 3'])
    else
        calibrationComplete = 1;
    end 
    set(handles.timerText,'visible','off')
end

if ~isempty(avgPress)
    set(handles.PressFrontLight,'BackgroundColor',[0,0,1]);
    set(handles.PressBagLight,'BackgroundColor',[0,0,1]);
    set(handles.PressBackLight,'BackgroundColor',[0,0,1]);
    
    set(handles.GyroBackLight,'BackgroundColor',[1,1,1]);
    set(handles.GyroFrontLight,'BackgroundColor',[1,1,1]);
else
    set(handles.PressFrontLight,'BackgroundColor',[1,0,1]);
    set(handles.PressBagLight,'BackgroundColor',[1,0,1]);
    set(handles.PressBackLight,'BackgroundColor',[1,0,1]);
    
    set(handles.GyroBackLight,'BackgroundColor',[1,0,1]);
    set(handles.GyroFrontLight,'BackgroundColor',[1,0,1]);
end
set(handles.timerText,'String',['Complete - Pressure levels: ',num2str(avgPress')])
pause(1)
handles.avgPress = avgPress;
handles.avgAcc_h = avgAcc_h;
guidata(hObject, handles);

% --- Executes on button press in CaliPresButton.
function calibrateButton1_Callback(hObject, eventdata, handles)
set(handles.timerText,'visible','on')
handles = setupValues(handles);
calibrationComplete = 0;
set(handles.timerText,'string','Autocalibrate, hold it perpendicular to the ground')
pause(3);
numberOfTries = 1;

handles.avgAcc= [0 0 0 0 0 0]';
while ~calibrationComplete
     
    [avgPress2, avgAcc, gyroCaliFlag] = autoCalibrateProbe(handles.serialobj);
    numberOfTries = 1+numberOfTries;


    if (length(avgPress2)<3 || abs(avgAcc(1))<13000||abs(avgAcc(1))>20000)
        %abs(avgAcc(1))<13000||abs(avgAcc(1))>20000)||
        set(handles.timerText,'String',['Something went wrong. We try again: ',num2str(numberOfTries),' of 3'])
    else
        calibrationComplete = 1;
    end 
    set(handles.timerText,'visible','off')
end

if ~isempty(avgPress2)

    
    set(handles.GyroBackLight,'BackgroundColor',[0,0,1]);
    set(handles.GyroFrontLight,'BackgroundColor',[0,0,1]);
else
    set(handles.PressFrontLight,'BackgroundColor',[1,0,0]);
    set(handles.PressBagLight,'BackgroundColor',[1,0,0]);
    set(handles.PressBackLight,'BackgroundColor',[1,0,0]);
    
    set(handles.GyroBackLight,'BackgroundColor',[1,0,0]);
    set(handles.GyroFrontLight,'BackgroundColor',[1,0,0]);
end
pause(1)
handles.avgAcc = avgAcc;
guidata(hObject, handles);



function CalibrateButton2_Callback(hObject, eventdata, handles)
set(handles.timerText,'visible','on')
handles = setupValues(handles);
calibrationComplete = 0;
set(handles.timerText,'string','Autocalibrate,Flip it')
pause(3);
numberOfTries = 1;
handles.avgPress1 = [0 0 0]';
handles.avgAcc1 = [0 0 0 0 0 0]';
while ~calibrationComplete
     
  
    numberOfTries = 1+numberOfTries;
      [avgPress1, avgAcc1, gyroCaliFlag1] = autoCalibrateProbe(handles.serialobj);

    if (length(avgPress1)<3 || abs(avgAcc1(1))<13000||abs(avgAcc1(1))>20000)
        %abs(avgAcc1(1))<13000||abs(avgAcc1(1))>20000) || 
        set(handles.timerText,'String',['Something went wrong. We try again: ',num2str(numberOfTries),' of 3'])
    else
        calibrationComplete = 1;
    end 
    set(handles.timerText,'visible','off')
end

if ~isempty(avgPress1)
    set(handles.PressFrontLight,'BackgroundColor',[0,1,0]);
    set(handles.PressBagLight,'BackgroundColor',[0,1,0]);
    set(handles.PressBackLight,'BackgroundColor',[0,1,0]);
    
    set(handles.GyroBackLight,'BackgroundColor',[0,1,0]);
    set(handles.GyroFrontLight,'BackgroundColor',[0,1,0]);
else
    set(handles.PressFrontLight,'BackgroundColor',[0,0,1]);
    set(handles.PressBagLight,'BackgroundColor',[0,0,1]);
    set(handles.PressBackLight,'BackgroundColor',[0,0,1]);
    
    set(handles.GyroBackLight,'BackgroundColor',[0,0,1]);
    set(handles.GyroFrontLight,'BackgroundColor',[0,0,1]);
end

pause(1)
handles.avgAcc1 = avgAcc1;

set(handles.returnButton, 'enable', 'off');
set(handles.timerText,'visible','off')
handles.examNo = Resources('getExamNr',handles.patientID);

Examination(handles.patientID, handles.patientName, handles.examNo, handles.avgPress,handles.serialobj, handles.patientComment, handles.avgAcc,handles.avgAcc1,handles.avgAcc_h);
delete(get(hObject, 'parent')); 

function skipButton_Callback(hObject, eventdata, handles)
set(handles.timerText,'visible','on')
handles = setupValues(handles);
pause(1)
calibrationComplete = 0;
numberOfTries = 1;
handles.avgPress = [0 0 0]';
handles.avgAcc1 = [0 0 0 0 0 0]';
while ~calibrationComplete
     
  
    numberOfTries = 1+numberOfTries;
      [avgPress, gyroCaliFlag1] = autoCalibrateProbe(handles.serialobj);

    if (length(avgPress)<3)
        %abs(avgAcc1(1))<13000||abs(avgAcc1(1))>20000) || 
        set(handles.timerText,'String',['Something went wrong. We try again: ',num2str(numberOfTries),' of 3'])
    else
        calibrationComplete = 1;
    end 
    set(handles.timerText,'visible','off')
end
handles.avgAcc = [17000 0 0 -17000 0 0];
handles.avgAcc1 = [17000 0 0 -17000 0 0];
handles.avgPress = avgPress;
handles.examNo = Resources('getExamNr',handles.patientID);
Examination(handles.patientID, handles.patientName,handles.examNo, handles.avgPress,handles.serialobj, handles.patientComment, handles.avgAcc,handles.avgAcc1,handles.avgAcc_h);
delete(get(hObject, 'parent')); %Closing this window


% Menu header --------------------------------------------------------------
function About_Callback(hObject, eventdata, handles)
About();

function Menu_Callback(hObject, eventdata, handles)

function Exit_Callback(hObject, eventdata, handles)
closeProgram();

function Comment_Callback(hObject, eventdata, handles)
PatientInput();

