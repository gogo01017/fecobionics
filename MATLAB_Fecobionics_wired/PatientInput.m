function varargout = PatientInput(varargin)
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @PatientInput_OpeningFcn, ...
    'gui_OutputFcn',  @PatientInput_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before PatientInput is made visible.
function PatientInput_OpeningFcn(hObject, eventdata, handles, varargin)
handles.patientID = varargin{1};
handles.selectExamination = varargin{2};
handles.windowCheck = varargin{3};
load('commentData.mat');

if isequal(handles.windowCheck,'examination') == 1
    set(handles.logTable, 'Data', commentData);
    set(handles.logTable, 'ColumnName', {'Time';'Comment'});
else
    filename = strcat(handles.patientID, '.mat');
    exPath = Resources('pathToExaminiations');
    comments = load([exPath filename{1}]);
    set(handles.logTable, 'Data', comments.data(handles.selectExamination).commentData.commentData);
    set(handles.deleteButton, 'enable', 'off')
end

global comment3global
global comment4global
global comment5global

set(handles.Comment3Text, 'String', comment3global);
set(handles.Comment4Text, 'String', comment4global);
set(handles.Comment5Text, 'String', comment5global);

% Choose default command line output for PatientInput
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = PatientInput_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles.output;

function getComment_CellSelectionCallback(hObject, eventdata, handles)
handles.index = eventdata.Indices;
guidata(hObject, handles);

% --- Executes on button press in CancelButton.
function CancelButton_Callback(hObject, eventdata, handles)
delete(get(hObject, 'parent')); %Closing this wind

% --- Executes on button press in deleteButton.
function deleteButton_Callback(hObject, eventdata, handles)
handles.output = deleteComment();
if strcmp(handles.output, 'Yes')
    commentData = get(handles.logTable, 'Data');
    commentData (handles.index(1), :) = [];
    set(handles.logTable, 'Data', commentData);
    fullPathToCommentData = Resources('fullPathToCommentData');
    save(fullPathToCommentData, 'commentData')
end

% --- Executes on button press in OKButton.
function OKButton_Callback(hObject, eventdata, handles)
global comment3global
global comment4global
global comment5global

comment3 = get(handles.Comment3Text, 'String');
comment4 = get(handles.Comment4Text, 'String');
comment5 = get(handles.Comment5Text, 'String');

comment3global = comment3;
comment4global = comment4;
comment5global = comment5;
delete(get(hObject, 'parent'));

function CommentTextInput_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function CommentTextInput_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
