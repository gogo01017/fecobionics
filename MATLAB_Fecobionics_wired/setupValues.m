function handles = setupValues(handles)
% Syntax:
%     handles = setupValues(handles);
%
% By FLU: 25/11-2015
% Version: 1.0 
handles.timeX = [];
handles.rotateAngle = [0 0 0 0 0 0]';
handles.pressure = [];
handles.gyroFront = [];
handles.gyroRear = [];
handles.gyroWTU = [];
handles.temperature = [];
handles.angleFront = [];
handles.angleRear = [];
handles.angleWTU = [];
handles.bend = [];
handles.bend_old = [];
handles.acc_angle = [];
handles.gy_angle = [];
handles.acc_gy_angle = [];
handles.acc_gy_angle1 = [];
handles.acc_gy_angle2 = [];
handles.acc_angle_x1 = [];
handles.acc_angle_y1 = [];
handles.acc_angle_z1 = [];
handles.acc1_angle = [];
handles.acc2_angle = [];

handles.angle_1= [];
handles.angle_2 = [];
handles.accFront = [];
handles.accRear = [];
handles.com.g_p_bit = 4/65535;
handles.com.m2s_p_bit = (4*9.79)/65535;
handles.com.grader_p_bit = 4000/65535;

