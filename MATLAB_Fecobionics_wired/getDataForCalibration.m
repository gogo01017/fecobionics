function [pressure, gyro, temp ,acc] = getDataForCalibration(serialObj,NumberOfSamplesToGet)
handles.com.dataOffsets.OFFSET_CHECKSUM_HB		= 98; %98
handles.com.dataOffsets.OFFSET_CHECKSUM_LB		= 99; %99
dataPacketReceived = 0;
numOfDataPackages = 0;
blockSize = 99;
dataPacket = zeros(blockSize, 1);
bytesAvail = serialObj.BytesAvailable;
pressure = [];
gyro = [];
temp = [];
acc = [];
%der skal kunne l�ses minimum en pakke ud 
%it is initialized.
tic
while NumberOfSamplesToGet*99 > bytesAvail
   pause(1)
   toc
   if toc>3
        disp('could not get enough samples for calibration')
       break
   end
    
end

if serialObj.BytesAvailable > NumberOfSamplesToGet*99
    %tager x antal pakker ud af gangen
    [intvalues,~,~]=fread(serialObj,NumberOfSamplesToGet*99,'uint8');
    numOfDataPackages = NumberOfSamplesToGet;
    %reshaped in packages
    intvalues = reshape(intvalues,99,numOfDataPackages);
    
    %If each datapackage is not starting with a AB (171) value it must
    %reinitialize (not made yet)
    if sum(intvalues(1,:)) == 171*numOfDataPackages
        
        %start data CheckSum calculations
        validatedPackages = 0;
        corruptPackages = [];
        for i = 1:numOfDataPackages
            CheckSumCalc = CheckSum(intvalues(:,i), blockSize-2);
            CheckSumBuf = [intvalues(handles.com.dataOffsets.OFFSET_CHECKSUM_LB,i) intvalues(handles.com.dataOffsets.OFFSET_CHECKSUM_HB,i)];
            CheckSumMsg = typecast(uint8(CheckSumBuf), 'UINT16');
            
            if (CheckSumCalc == CheckSumMsg)
                switch intvalues(3,i);
                    case 5
                        validatedPackages = validatedPackages + 1;
                   case 20
                        %stopCommand();
                       msgbox('The connection is lost. The examination is therefor stoped', 'error', 'error')
                    case 21 || 22 || 23 || 24 || 25 || 26
                        %corruptPackages  = [corruptPackages i];
                        %error = intvalues(3,i);
                        %f = Error(error);
                        %waitfor(f);
                        validatedPackages = validatedPackages + 1;
                end
            else
                corruptPackages  = [corruptPackages i];
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %Not taken care of corruptPackages
        if ~isempty(corruptPackages)
            f = msgbox('Corupt pacakages was found. This is an message in relation to tests.', 'error', 'error');
            uiwait(f)
            
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %starts the data extraction
        if isequal(validatedPackages,numOfDataPackages)
            
            [timeSample,gyro,gyroSampleTime,pressure,temp,acc] = unpackData(intvalues);
            
        else
            corruptPackages
            numOfDataPackages
            disp('Error package, line 98, serial call back')
            msgbox('The connection is lost. The examination is therefor stoped', 'error', 'error')
            %stopCommand();
            
            %must reinitialize, e.g. device stopped.
        end    
    end
else
    disp('No data')
end