function [serialobj, successFlag] = initializeSerial(serialobj)
numOfDataPackage = floor(serialobj.BytesAvailable/99);
numberOfRetries = 0;

%Tries to start the feco
while numberOfRetries < 5  &&  numOfDataPackage<3
    startCommand(serialobj);
    if numberOfRetries > 3
        p = msgbox('Trying to establish connection to probe');
        pause(2)
        delete(p)   
    else
        pause(4)
    end
    numOfDataPackage = floor(serialobj.BytesAvailable/99);
    numberOfRetries = numberOfRetries + 1;
end


if numberOfRetries < 5
    %initializering er g�et godt
    numOfDataPackage = floor(serialobj.BytesAvailable/99);
    if serialobj.BytesAvailable < 1000
        pause(2)
        disp('pause to initialize')
    end
    
    [intvalues,~,~]=fread(serialobj,1000,'uint8');
    
    idx = find(intvalues==hex2dec('AB'));
    idxStart = find(diff(idx)==99);      
    idxStart = idx(idxStart(end))+99; %for det er en diff, og s?er den sidste i r�kken ikke med.
    extraBytes = 99-(1000-idxStart)-1;
    if extraBytes > 0
    [~,~,~]=fread(serialobj,extraBytes,'uint8');
    end
    successFlag = 1;
    
    %perhaps some error capturing..
    
else
    %initializeringen er g�et d�rligt
    successFlag = 0;
end