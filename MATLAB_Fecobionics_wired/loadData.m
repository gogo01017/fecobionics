function loadData(handles)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
%flu: upadte for unspecific paths. 
filename = strcat(handles.patientID, '.mat');
pathToExaminiations = Resources('pathToExaminiations');
patientData = load([pathToExaminiations filename]);
if handles.selectExamination ~= 0
    examinationData = patientData.data(handles.selectExamination);
    set([handles.anglePlot; handles.pressPlot],'xlim',[examinationData.timeX(1)-0.01 examinationData.timeX(end)+1]);
    
    %update the graphs
    set(handles.pressurefrontplot,'Ydata',examinationData.pressure(1,:),'Xdata', examinationData.timeX);
    set(handles.pressureballonplot,'Ydata',examinationData.pressure(2,:),'Xdata', examinationData.timeX);
    set(handles.pressurebackplot,'Ydata',examinationData.pressure(3,:),'Xdata', examinationData.timeX);
    
    set(handles.angleBendPlot, 'Ydata', examinationData.angleBend, 'Xdata', examinationData.timeX);
    
    %set labels
    set(handles.volumeText, 'String', examinationData.volume);
    set(handles.maxPressFrontText, 'String', round(max(examinationData.pressure(1,:)),1));
    set(handles.maxPressBagText, 'String', round(max(examinationData.pressure(2,:)),1));
    set(handles.maxPressBackText, 'String', round(max(examinationData.pressure(3,:)),1));
    set(handles.tempText, 'String', round(mean(examinationData.temperature),1));
    
%    set(handles.acc_angleText, 'String', round(max(handles.acc_angle(startIdx:end)),1));
%   set(handles.gy_angleText, 'String', round(max(handles.gy_angle(startIdx:end)),1));
  
    set(handles.angleMinText, 'String', round(min(examinationData.angleBend),1));
    set(handles.angleMaxText, 'String', round(max(examinationData.angleBend),1));
    
    linkaxes([handles.pressPlot, handles.anglePlot],['x']);
end


