function varargout = Info(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Info_OpeningFcn, ...
    'gui_OutputFcn',  @Info_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end


% --- Executes just before Info is made visible.
function Info_OpeningFcn(hObject, ~, handles, varargin)
handles.output = hObject;
movegui(gcf,'center');

[handles] = loadPatientTable(handles);
handles.selectedpatientname = '';
handles.selectedpatientid = '';

commentData = {'' ''};
fullPathToCommentData = Resources('fullPathToCommentData');
save(fullPathToCommentData, 'commentData')
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = Info_OutputFcn(~, ~, handles)
varargout{1} = handles.output;

function getPatient_CellSelectionCallback(hObject, eventdata, handles)
index = eventdata.Indices;
if isequal(eventdata.EventName,'CellSelection')
    if ~isempty(index)
        data = formatUItable(handles.patientTableData, index);
        set(handles.patientTable,'data',data)
    end
end

if ~isempty(index)
    handles.selectedpatientname = handles.patientTableData{index(1),1};
    handles.selectedpatientid = handles.patientTableData{index(1),2};
    handles.selectedpatientcomment = handles.patientTableData{index(1),5};
end

checkAvalibleData(handles);
guidata(hObject, handles);

% --- Executes on button press in stopButton.
function stopButton_Callback(~, ~, ~)

% --- Executes on button press in anaysisButton.
function anaysisButton_Callback(hObject, ~, handles)
handles.patientName = handles.selectedpatientname;
handles.patientID = handles.selectedpatientid;

Analysis(handles.patientID, handles.patientName); %Opning patient gui
delete(get(hObject, 'parent')); %Closing this window


% --- Executes on button press in setupButton.
% start examination jump to the setup function
function startButton_Callback(hObject, ~, handles)
handles.patientName = handles.selectedpatientname;
handles.patientID = handles.selectedpatientid;
handles.patientComment = handles.selectedpatientcomment;

Setup(handles.patientID, handles.patientName, handles.patientComment); %Opning patient gui
delete(get(hObject, 'parent')); %Closing this window

function newPatientButton_Callback(hObject, ~, handles)
f = newPatient(); %Opning patient gui
waitfor(f);
[handles] = loadPatientTable(handles);
guidata(hObject, handles);

function timeTB_Callback(~, ~, ~)

% --- Executes during object creation, after setting all properties.
function timeTB_CreateFcn(hObject, ~, ~)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function idPatTB_Callback(~, ~, ~)

% --- Executes during object creation, after setting all properties.
function idPatTB_CreateFcn(hObject, ~, ~)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function namePatTB_Callback(~, ~, ~)

% --- Executes during object creation, after setting all properties.
function namePatTB_CreateFcn(hObject, ~, ~)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% Menu --------------------------------------------------------------------
function Menu_Callback(~, ~, ~)
%not needed. 

function About_Callback(~, ~, ~)
About();

function Exit_Callback(~, ~, ~)
closeProgram();

function Comment_Callback(~, ~, ~)
PatientInput();

function reloadPatientData_Callback(hObject, eventdata, handles)
ButtonName = questdlg('Do you really want to reload all patient data?', ...
    'Reload patient data', ...
    'Yes', 'No', 'No');
switch ButtonName,
    case 'Yes',
        pathToTemp = Resources('pathToExaminiations');  
        exFiles = dir([pathToTemp,'*.mat'])
        if ~isempty(exFiles)
        for i = 1:length(exFiles)
            p = load([pathToTemp exFiles(i).name]);
            
        end
        [handles] = loadPatientTable(handles);
        handles.selectedpatientname = '';
        handles.selectedpatientid = '';

commentData = {'' ''};
fullPathToCommentData = Resources('fullPathToCommentData');
save(fullPathToCommentData, 'commentData')
guidata(hObject, handles);
        else
            f = msgbox('Not files found');
            waitfor(f)
            
        end
        
end % switch

function deleteTempData_Callback(hObject, eventdata, handles)
pathToTemp = Resources('pathToTemp');
numFiles = dir([pathToTemp ,'*.mat']);
bytes = 0;
for i = 1:length(numFiles)
    bytes = bytes+numFiles(i).bytes;
end
mbytes = round(bytes/1024000,1);
ButtonName = questdlg(['Do you really want to delete all ',num2str(length(numFiles)),' temporary files (~',num2str(mbytes) 'MB)?'], ...
    'Delete temporary data', ...
    'Yes', 'No', 'No');
switch ButtonName,
    case 'Yes',
       for i = 1:length(numFiles)
           delete([pathToTemp, numFiles(i).name])
       end     
       msgbox('All deleted succesfully')
end % switch
