function [Cheksum, sum1, sum2] = CheckSum(buf, NoOfBytes)
%calculate the checksum...

sum1 = zeros(1,1,'uint16'); 
sum2 = zeros(1,1,'uint16');

for i = 1:NoOfBytes
  
  sum1 = mod((sum1 + buf(i)),255);
  sum2 = mod((sum2 + sum1),255);
    
end

Cheksum = typecast(uint8([sum1 sum2]), 'UINT16');