function  gyro_angle = gy2angle(gyro1, gyro2, timeSample)
global gyro_angle_x;
global gyro_angle_y;
global gyro_angle_z;
  if (isempty(gyro_angle_x) ==0)
gyro_angle_x(1) = gyro_angle_x(end);
gyro_angle_y(1) = gyro_angle_x(end);
gyro_angle_z(1) = gyro_angle_x(end);
gyro_angle_x(2:end) = 0;
gyro_angle_y(2:end) = 0;
gyro_angle_z(2:end) = 0;
end
numbOfPackages = size(gyro1,2);
  if (isempty(gyro_angle_x) ==1)
gyro_angle_x = zeros(numbOfPackages,1);
gyro_angle_y = zeros(numbOfPackages,1);
gyro_angle_z = zeros(numbOfPackages,1);
  end
cos_gyro = zeros(numbOfPackages,1);
gyro_angle = zeros(numbOfPackages,1);
    gx = gyro1(2,:) - gyro2(2,:);
    gy = gyro1(3,:) - gyro2(3,:);
    gz = gyro1(4,:) - gyro2(4,:);
for i = 1:numbOfPackages-1
    gyro_angle_x(i+1) = gyro_angle_x(i) + gx(i)*(timeSample(i+1)-timeSample(i));
    gyro_angle_y(i+1) = gyro_angle_y(i) + gy(i)*(timeSample(i+1)-timeSample(i));
    gyro_angle_z(i+1) = gyro_angle_z(i) + gz(i)*(timeSample(i+1)-timeSample(i));
    cos_gyro(i) = cos((gyro_angle_x(i))/2*pi/180)*cos((gyro_angle_z(i))/2*pi/180)+sin((gyro_angle_x(i))/2*pi/180)*sin((gyro_angle_z(i))/2*pi/180);
    gyro_angle(i) = 2*acos(cos_gyro(i))*180/pi;
end

