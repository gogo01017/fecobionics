function varargout = Analysis(varargin)
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Analysis_OpeningFcn, ...
    'gui_OutputFcn',  @Analysis_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Analysis is made visible.
function Analysis_OpeningFcn(hObject, eventdata, handles, varargin)
movegui(gcf,'center');

handles.patientID = varargin{1};
set(handles.patientIDText, 'String', handles.patientID);
handles.patientName = varargin{2};
set(handles.patientNameText, 'String', handles.patientName);

degree = sprintf('%c', char(176));
set(handles.celText, 'String', [degree ' Celcius']);

loadMenu(handles);

[angleBendPlot] = createAnglePlot(handles);
handles.angleBendPlot = angleBendPlot;

[pressureballonplot, pressurefrontplot, pressurebackplot] = createPressurePlot(handles);
handles.pressureballonplot = pressureballonplot;
handles.pressurefrontplot = pressurefrontplot;
handles.pressurebackplot = pressurebackplot;

% Choose default command line output for Analysis
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = Analysis_OutputFcn(hObject, eventdata, handles)
% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in returnButton.
function returnButton_Callback(hObject, eventdata, handles)
Info(handles.patientID, handles.patientName);  %Opning patient gui -handles.patientID, handles.patientName
delete(get(hObject, 'parent')); %Closing this window

% --- Executes on button press in exportButton.
function exportButton_Callback(hObject, eventdata, handles)
Export(handles,'all');

function exportSectionButton_Callback(hObject, eventdata, handles)
Export(handles,'section');

% --- Executes on button press in commentButton.
function commentButton_Callback(hObject, eventdata, handles)
handles.windowCheck = 'analysis';
PatientInput(handles.patientID, handles.selectExamination);

% --- Executes on selection change in examinMenu.
function examinMenu_Callback(hObject, eventdata, handles)
selectExamination = get(handles.examinMenu,'value');
if selectExamination > 1
    handles.selectExamination = selectExamination-1;
    loadData(handles);
    commentLinesAnalysis(handles);
    guidata(hObject, handles);
    
    set(handles.commentButton, 'enable' , 'on');
    set(handles.exportButton, 'enable' , 'on');
    set(handles.exportSectionButton, 'enable' , 'on');
else
    set(handles.commentButton, 'enable' , 'off');
    set(handles.exportButton, 'enable' , 'off');
    set(handles.exportSectionButton, 'enable' , 'off');
end


% --- Executes during object creation, after setting all properties.
function examinMenu_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
function CommentHeader_Callback(hObject, eventdata, handles)

function Comment_Callback(hObject, eventdata, handles)
PatientInput();

function Exit_Callback(hObject, eventdata, handles)
closeProgram();

function About_Callback(hObject, eventdata, handles)
About();
