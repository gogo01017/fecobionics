function [pressureballonplot, pressurefrontplot, pressurebackplot] = createPressurePlot(handles)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
axes(handles.pressPlot)

pressurefrontplot=plot(0,0, 'b','linewidth',2); 
hold on
pressureballonplot=plot(0,0,'g','linewidth',2);
pressurebackplot=plot(0,0, 'r','linewidth',2);

ylim([-10 180]) 
legend('Front pressure','Bag pressure','Rear pressure', 'location', 'northwest'); 
set(handles.pressPlot, 'YAxisLocation', 'right');
xlabel('Time[s]');
ylabel('cm H_2O');

end

