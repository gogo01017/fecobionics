function varargout = Comment6(varargin)
% COMMENT6 MATLAB code for Comment6.fig
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Comment6_OpeningFcn, ...
                   'gui_OutputFcn',  @Comment6_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Comment6 is made visible.
function Comment6_OpeningFcn(hObject, ~, handles, varargin)
handles.output = hObject;
time = varargin{1};
set(handles.TimeText, 'String', time);
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Comment6 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Comment6_OutputFcn(~, ~, handles) 
varargout{1} = handles.output;


% --- Executes on button press in OKButton.
function OKButton_Callback(hObject, ~, handles)
load('commentData');
if ~isempty(handles.TimeText) && ~isempty(handles.Comment6Text)
    time = get(handles.TimeText, 'String');
    comment = get(handles.Comment6Text, 'String');
    
    insert = matfile('commentData.mat', 'Writable', true);
    
    newComment = {time, comment};
    insert.commentData(end+1,:) = newComment;
end
delete(get(hObject, 'parent'));

% --- Executes on button press in CancelButton.
function CancelButton_Callback(hObject, ~, ~)
delete(get(hObject, 'parent'));

function TimeText_Callback(~, ~, ~)

% --- Executes during object creation, after setting all properties.
function TimeText_CreateFcn(hObject, ~, ~)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function Comment6Text_Callback(~, ~, ~)

% --- Executes during object creation, after setting all properties.
function Comment6Text_CreateFcn(hObject, ~, ~)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end