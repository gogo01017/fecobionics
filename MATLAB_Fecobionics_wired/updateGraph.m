function updateGraph(hObject,handles)
% Syntax:
%     updateGraph(hObject,handles)
%
% Version 2.0
% Updated 26/11-2015

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Finds the window to plot %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if handles.timeX(end)<60
    startIdx = 1;
else
    startIdx = floor(size(handles.timeX,2)-handles.sampleFreq*60+1);
    if startIdx < 1
        startIdx = 1;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% pressure graph %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(handles.pressurefrontplot, 'Ydata', handles.pressure(1,startIdx:end), 'Xdata', handles.timeX(startIdx:end));
set(handles.pressureballonplot, 'Ydata', handles.pressure(2,startIdx:end), 'Xdata', handles.timeX(startIdx:end));
set(handles.pressurebackplot, 'Ydata', handles.pressure(3,startIdx:end), 'Xdata', handles.timeX(startIdx:end));
try
    set([handles.anglePlot; handles.pressPlot; handles.FrontRearPlot;],'xlim',[handles.timeX(startIdx)-0.01 handles.timeX(end)+1])
catch 
    disp('Du er en l�rt')
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% angle graph %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(handles.angleBend, 'Ydata', handles.bend(startIdx:end), 'Xdata', handles.timeX(startIdx:end));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% front rear graph %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(handles.position1, 'Ydata', handles.acc_gy_angle1(startIdx:end), 'Xdata', handles.timeX(startIdx:end));
set(handles.position2, 'Ydata', handles.acc_gy_angle2(startIdx:end), 'Xdata', handles.timeX(startIdx:end));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Feco bend plot %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~isnan(handles.bend(end))
    idx = find(round(handles.bend(end))==handles.transfer.angle(1,:));
    if isempty(idx)
        set(handles.fecoAnglePlot,'color',[.8 .8 .8])
    else
        set(handles.fecoAnglePlot, 'Ydata', handles.transfer.x{idx}, 'Xdata', handles.transfer.y{idx},'color','r');
        %det er med vilje x og y er byttet rundt for ellers plottes det ikke
        %verticalt :)
    end
else
       set(handles.fecoAnglePlot,'color',[0.9 .9 .9])
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% Set labels and colors %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
set(handles.TempText, 'String', round(mean(mean(handles.temperature(startIdx:end))),1));
set(handles.PressFrontText, 'String', round(max(handles.pressure(1,startIdx:end)),1));
set(handles.PressBagText, 'String', round(max(handles.pressure(2,startIdx:end)),1));
set(handles.PressBackText, 'String', round(max(handles.pressure(3,startIdx:end)),1));

set(handles.angleMinText, 'String', round(min(handles.bend(startIdx:end)),1));
set(handles.angleMaxText, 'String', round(max(handles.bend(startIdx:end)),1));

 set(handles.acc_angleText, 'String', round(handles.acc_angle(end)));
  set(handles.gy_angleText, 'String', round(handles.gy_angle(end)));
  
set(handles.acc_x_front_text, 'String', round(handles.accFront(1,end)));
set(handles.acc_y_front_text, 'String', round(handles.accFront(2,end))-(handles.avgAcc(2)+handles.avgAcc1(2))/2);
set(handles.acc_z_front_text, 'String', round(handles.accFront(3,end))-(handles.avgAcc(3)+handles.avgAcc1(3))/2);
set(handles.acc_x_rear_text, 'String', round(handles.accRear(1,end)));
set(handles.acc_y_rear_text, 'String', round(handles.accRear(2,end))-(handles.avgAcc(5)+handles.avgAcc1(5))/2);
set(handles.acc_z_rear_text, 'String', round(handles.accRear(3,end))-(handles.avgAcc(6)+handles.avgAcc1(6))/2);
% set(handles.acc1_text, 'String', round(handles.acc1_angle(end)));
% set(handles.acc2_text, 'String', round(handles.acc2_angle(end)));
% set(handles.angle_1_text, 'String', round(handles.angle_1(end)));
% set(handles.angle_2_text, 'String', round(handles.angle_2(end)));
if (handles.pressure(3,end) > 120 || handles.pressure(3,end) < -10)
    set(handles.surfHandle1, 'FaceColor',[1 1 1]);
else
    set(handles.surfHandle1, 'FaceColor',handles.color(floor(handles.pressure(3,end))+11,:));
end

if (handles.pressure(2,end) > 120 || handles.pressure(2,end) < -10)
    set(handles.surfHandle3, 'FaceColor',[1 1 1]);
else
    set(handles.surfHandle3, 'FaceColor',handles.color(floor(handles.pressure(2,end))+11,:));
end

if (handles.pressure(1,end) > 120 || handles.pressure(1,end) < -10)
    set(handles.surfHandle2, 'FaceColor',[1 1 1]);
else
    set(handles.surfHandle2, 'FaceColor',handles.color(floor(handles.pressure(1,end))+11,:));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% Create temp files for backup %%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
timer = toc;
if timer > 5
   dateString = saveTempFile(handles);
   %Also print out the frequency. 
   meanFreq = length(handles.timeX(startIdx:end))/(handles.timeX(end)-handles.timeX(startIdx));

   set(handles.InfoTextBox,'string',[num2str(round(meanFreq,1)),'Hz, Last temp file saved: ',dateString])
   tic;
end

if timer > 2 && timer < 5 
     set(handles.InfoTextBox,'string','')
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%% Draw and update handles %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
drawnow();
pause(0.01) %else the graph has no time to update
guidata(hObject, handles)
