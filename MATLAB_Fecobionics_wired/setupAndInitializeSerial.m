function [serialobj, successFlag] = setupAndInitializeSerial()
serialobj = [];
successFlagCreateSerial = 0;
successFlagInitialize = 0;
while 1
    [serialobj, successFlagCreateSerial] = createSerialPort();
    if successFlagCreateSerial
        handles.serialobj = serialobj;
        startCommand(handles.serialobj);
        [handles.serialobj, successFlagInitialize] = initializeSerial(handles.serialobj);
        %stopCommand(serialobj);
        
        if successFlagInitialize
            %stopCommand(handles.serialobj)
            break
        else
            ButtonName = questdlg('Could not initialize the probe. Do you want to try again?', ...
                'Initialize problems', ...
                'Yes', 'No', 'Yes');
            switch ButtonName,
                case 'Yes',
                    
                otherwise %s?er det ogs?nej hvis man lukke boksen
                    successFlagInitialize = 0;
                    break;
            end % switch
        end
       
   else
        ButtonName = questdlg('Could not find a serial port. Do you want to try again?', ...
            'Comm port not found', ...
            'Yes', 'No', 'Yes');
        switch ButtonName,
            case 'Yes',
                
            otherwise 'No',
                successFlagCreateSerial = 0;
                break;
        end % switch
    end
end

successFlag = successFlagCreateSerial && successFlagInitialize;