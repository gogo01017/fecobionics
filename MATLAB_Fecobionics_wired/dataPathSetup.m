function varargout = dataPathSetup(varargin)
% DATAPATHSETUP MATLAB code for dataPathSetup.fig
%      DATAPATHSETUP, by itself, creates a new DATAPATHSETUP or raises the existing
%      singleton*.
%
%      H = DATAPATHSETUP returns the handle to a new DATAPATHSETUP or the handle to
%      the existing singleton*.
%
%      DATAPATHSETUP('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DATAPATHSETUP.M with the given input arguments.
%
%      DATAPATHSETUP('Property','Value',...) creates a new DATAPATHSETUP or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before dataPathSetup_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to dataPathSetup_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help dataPathSetup

% Last Modified by GUIDE v2.5 27-Nov-2015 13:46:23

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @dataPathSetup_OpeningFcn, ...
                   'gui_OutputFcn',  @dataPathSetup_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before dataPathSetup is made visible.
function dataPathSetup_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to dataPathSetup (see VARARGIN)

% Choose default command line output for dataPathSetup
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes dataPathSetup wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = dataPathSetup_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in okButton.
function okButton_Callback(hObject, eventdata, handles)
% hObject    handle to okButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in cancleButton.
function cancleButton_Callback(hObject, eventdata, handles)
% hObject    handle to cancleButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
