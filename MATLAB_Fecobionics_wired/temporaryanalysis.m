%load af temp filer
clear all
close all
figure
paths = dir('C:\Users\Leon\Desktop\FecoHans\*.mat');
for i = 1:length(paths)
    subplot(3,4,i)
    p = load(['C:\Users\Leon\Desktop\FecoHans\',paths(i).name]);
    
    plot(p.data.time,p.data.pressure)
    if i == 1
    legend({'PressureFront';'PressureBag';'PressureRear'},'location','northwest')
    end
    hold on
    if ~isempty(p.data.commentData.commentData)
        changeFlag = 0;
        
        for j = 1:size(p.data.commentData.commentData,1)
            if isequal(p.data.commentData.commentData{j,1},'')
                changeFlag = 1;
            end
        end
        if changeFlag
            p.data.commentData.commentData = p.data.commentData.commentData(2:end,:);
        end
        if ~isempty(p.data.commentData.commentData)
            
            for j = 1:size(p.data.commentData.commentData,1)
                commentsTime = str2num(cell2mat(p.data.commentData.commentData(j,1)));
                foo = ylim;
                plot([commentsTime commentsTime],foo);
            end
            title(p.data.commentData.commentData(:,2))
        else
            str = strrep(paths(i).name,'_',' Temp #:');
            title(str)
        end

        
    end
        ylim([-20 120])   
        p.data.volume
end

