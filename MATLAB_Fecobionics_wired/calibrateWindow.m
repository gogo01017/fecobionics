function varargout = calibrateWindow(varargin)
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @calibrateWindow_OpeningFcn, ...
                   'gui_OutputFcn',  @calibrateWindow_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before calibrateWindow is made visible.
function calibrateWindow_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;

axes(handles.image);
imshow('caliProbe.png')

% Update handles structure
guidata(hObject, handles);

% --- Outputs from this function are returned to the command line.
function varargout = calibrateWindow_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

% --- Executes on button press in okButton.
function okButton_Callback(hObject, eventdata, handles)
delete(get(hObject, 'parent'));


