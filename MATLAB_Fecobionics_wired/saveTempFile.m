function timeOfSave = saveTempFile(handles)
fullPathToCommentData = Resources('fullPathToCommentData');
commentData = load(fullPathToCommentData);

data.patient.patientID  = handles.patientID;
data.patient.patientName  = handles.patientName;
data.patient.patientComment = handles.patientComment;

data.timeX      = handles.timeX;
data.gyroFront  = handles.gyroFront;
data.gyroRear   = handles.gyroRear;
data.gyroWTU    = handles.gyroWTU;
data.accFront  = handles.accFront;
data.accRear   = handles.accRear;
data.acc_angle  = handles.acc_angle;
data.gy_angle  = handles.gy_angle;
data.pressure   = handles.pressure;

data.temperature= handles.temperature;

data.angleFront = handles.angleFront;
data.angleRear  = handles.angleRear;
data.angleWTU   = handles.angleWTU;
data.angleBend  = handles.bend;
    data.acc_gy_angle_front = handles.acc_gy_angle1;
    data.acc_gy_angle_rear = handles.acc_gy_angle2;
data.examNo     = num2str(handles.examNo);
data.tempTime   = datestr(now, 'dd.mm.yyyy HH.MM.SS');
data.startTime  = handles.startTime;
data.volume     = get(handles.volumeText, 'UserData');
data.commentData= commentData(:,:);

pathToTemp = Resources('pathToTemp');
TempNo = Resources('getTempNo',handles.patientID);
filename = strcat(pathToTemp, 'temp_', handles.patientID, '.EX',num2str(handles.examNo),'.', num2str(TempNo), '.mat');
handles.examNo
save(filename, 'data');
%Resources('updateTempNo');

timeOfSave = data.tempTime;